

/****** Object:  Table [dbo].[plat_engine_webpage_custom_page]    Script Date: 2016/5/27 18:40:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[plat_engine_webpage_custom_page](
	[PageID] [varchar](50) NOT NULL CONSTRAINT [DF_plat_engine_webpage_custom_page_PageID]  DEFAULT (newid()),
	[PageTitle] [varchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL CONSTRAINT [DF_plat_engine_webpage_custom_page_CreateTime]  DEFAULT (getdate()),
	[UpdateTime] [datetime] NOT NULL CONSTRAINT [DF_plat_engine_webpage_custom_page_UpdateTime]  DEFAULT (getdate()),
	[PageHtml] [text] NULL CONSTRAINT [DF_plat_engine_webpage_custom_page_PageHtml]  DEFAULT (''),
 CONSTRAINT [PK_plat_engine_webpage_custom_page] PRIMARY KEY CLUSTERED 
(
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'页面ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_custom_page', @level2type=N'COLUMN',@level2name=N'PageID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'页面标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_custom_page', @level2type=N'COLUMN',@level2name=N'PageTitle'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_custom_page', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_custom_page', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'页面内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_custom_page', @level2type=N'COLUMN',@level2name=N'PageHtml'
GO


