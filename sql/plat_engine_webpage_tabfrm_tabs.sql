
/****** Object:  Table [dbo].[plat_engine_webpage_tabfrm_tabs]    Script Date: 2016/5/27 18:40:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[plat_engine_webpage_tabfrm_tabs](
	[TabID] [varchar](50) NOT NULL CONSTRAINT [DF_plat_engine_webpage_tabfrm_tabs_TabID]  DEFAULT (newid()),
	[TabTitle] [varchar](50) NOT NULL,
	[TabUrl] [varchar](50) NOT NULL,
	[TabHeight] [varchar](50) NOT NULL CONSTRAINT [DF_plat_engine_webpage_tabfrm_tabs_TabHeight]  DEFAULT ('100%'),
	[TabWidth] [varchar](50) NOT NULL CONSTRAINT [DF_plat_engine_webpage_tabfrm_tabs_TabWidth]  DEFAULT ('100%'),
	[TabClose] [int] NOT NULL CONSTRAINT [DF_plat_engine_webpage_tabfrm_tabs_TabClose]  DEFAULT ((1)),
	[OrderID] [varchar](50) NULL,
	[RefTabFrmID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_plat_engine_webpage_tabfrm_tabs_TabID] PRIMARY KEY CLUSTERED 
(
	[TabID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标签页ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm_tabs', @level2type=N'COLUMN',@level2name=N'TabID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标签页名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm_tabs', @level2type=N'COLUMN',@level2name=N'TabTitle'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'链接地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm_tabs', @level2type=N'COLUMN',@level2name=N'TabUrl'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'高度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm_tabs', @level2type=N'COLUMN',@level2name=N'TabHeight'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宽度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm_tabs', @level2type=N'COLUMN',@level2name=N'TabWidth'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'可关闭' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm_tabs', @level2type=N'COLUMN',@level2name=N'TabClose'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'引用标签页框架ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm_tabs', @level2type=N'COLUMN',@level2name=N'RefTabFrmID'
GO


