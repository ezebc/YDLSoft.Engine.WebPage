

/****** Object:  Table [dbo].[plat_engine_webpage_tabfrm]    Script Date: 2016/5/27 18:39:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[plat_engine_webpage_tabfrm](
	[TabFrmID] [varchar](50) NOT NULL CONSTRAINT [DF_plat_engine_webpage_tabfrm_TabFrmID]  DEFAULT (newid()),
	[TabFrmName] [varchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL CONSTRAINT [DF_plat_engine_webpage_tabfrm_CreateTime]  DEFAULT (getdate()),
	[UpdateTime] [datetime] NOT NULL CONSTRAINT [DF_plat_engine_webpage_tabfrm_UpdateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_plat_engine_webpage_tabfrm] PRIMARY KEY CLUSTERED 
(
	[TabFrmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标签页框架ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm', @level2type=N'COLUMN',@level2name=N'TabFrmID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标签页框架说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm', @level2type=N'COLUMN',@level2name=N'TabFrmName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'plat_engine_webpage_tabfrm', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO


