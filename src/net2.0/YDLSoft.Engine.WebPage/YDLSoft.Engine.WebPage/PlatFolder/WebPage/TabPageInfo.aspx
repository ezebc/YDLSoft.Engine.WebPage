﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TabPageInfo.aspx.cs" Inherits="YDLSoft.Engine.WebPage.PlatFolder.WebPage.TabPageInfo" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>标签页设置</title>
    <link href="../../themes/default/skin.css" rel="stylesheet" />
    <link href="../../themes/icon.css" rel="stylesheet" />
    <link href="../../Scripts/sticky/jquery.sticky.min.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src="../../Scripts/common/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="head" >
        <div style="text-align:right;padding-right:20px;">
            <input id="btn_save" type="button" value="保存" name="btn_save" onclick="system_saveData();" />
            <input id="btn_preview" style="display:none" type="button" value="预览" name="btn_save" onclick="system_preview();" />
        </div>
    </div>
    <div id="content">
		<div class="con">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table">
				<tr>
					<td colspan="4" class="t">标签页</td>
				</tr>
				<tr style="display:none">
					<td class="l u">标签页ID</td>
					<td class="l" colspan="3">
                        <input id="TabFrmID" name="TabFrmID" class="form-control" readonly="readonly" value="" /> 
                    </td>					
				</tr>
				<tr>
                    <td class="l u" style="width:15%;">标签页说明</td>
                    <td class="l" style="width:35%;"><input id="TabFrmName" name="TabFrmName" class="form-control require" type="text" value="" /></td>
				</tr>
			</table>

            <table cellpadding="0" cellspacing="0" border="0" class="subTableForm" style="width:100%;" align="center">
                <thead>
                       <tr class="form-item-sub" style="font-weight:bold;">
                           <td style="display:none">记录ID</td>
                           <td style="width:50px;text-align:center">排序<span style="color:red;font-weight:bold"> *</span></td>
                           <td style="width:15%;text-align:center">标题名称<span style="color:red;font-weight:bold"> *</span></td>
                           <td style="width:60%;text-align:center">链接地址</td>
                           <td style="width:100px;text-align:center">高度</td>
                           <td style="width:100px;text-align:center">宽度</td>
                           <td style="width:120px;text-align:center">可关闭</td>
                           <td style="width:50px;text-align:center">
                               <span class="icon-add icon-center"  onclick="addRow();"></span>
                           </td>
                       </tr>
                </thead>
                <tbody id="list">
                    
                </tbody>
             </table>
		</div>
	</div>   
</body>
</html>
<script src="../../Scripts/jQuery/jquery.min.js"></script>
<script src="../../Scripts/sticky/jquery.sticky.min.js"></script>
<script src="../../../Scripts/common/ydlsoft.js"></script>
<script src="../../Scripts/layer/layer.js"></script>
<script src="../../Scripts/nodetpl/nodetpl.client.min.js"></script>
<script type="text/javascript">
    var ydlSoft = new YDLSoft();
    var _dataId = ydlSoft.GetQueryString("TabFrmID");
    //默认是修改
    var _isNew = "0";
    var loadIndex = -1;
    $(document).ready(function(){        
        if (_dataId) {
            _isNew = "0";
            document.getElementById("btn_preview").style.display = "";
            loadDataInfo(_dataId);
        } else {
            _isNew = "1";            
            addRow();
            addRow();
            addRow();
            document.getElementById("TabFrmName").focus();
        }
    });
    $(document).keydown(function (e) {
        //ctrl+enter
        if (e.ctrlKey && event.keyCode == 13) {
            addRow();
        }
        //ctrl+s
        if (e.ctrlKey && event.keyCode == 83) {
            system_saveData();
            return false;
        }
    });

    var rowsCnt = 0;
    function addRow() {
        rowsCnt = rowsCnt + 1;
        var data_tpl = { list: [{ TabID: "", TabTitle: "", TabUrl: "", TabHeight: "100%", TabWidth: "100%", OrderID: rowsCnt,TabClose:0 }], state: "add" };
        //console.log(data_tpl);
        var content = document.getElementById('t_list').innerHTML;
        nodetpl.render(content, data_tpl, function (d) {
            //console.log(d);
            jQuery("#list").append(d);
            document.getElementById("txt_TabTitle_" + rowsCnt).focus();
        });
    }

    function delRow(oindex) {
        var objTr = jQuery("#tr_" + oindex);
        var tr_state = objTr.attr("state");
        if (tr_state == "add") {
            //如果是客户端新增的行数据直接删除元素
            objTr.remove();
        } else {
            objTr.hide();
            //设置数据状态
            objTr.attr("state", "del");
        }
    }


    //加载表单信息编辑
    function loadDataInfo(dataId)
    {
        //加载层-风格4
        loadIndex = layer.msg('正在加载，请稍后', { icon: 16, shade: [0.8, '#393D49'] });
        jQuery.ajax({
            url: "DataService/TabPageService.ashx?method=GetTabPageInfo&TabFrmID=" + dataId,
            type: "post",
            success: function (text) {
                var dataObj = jQuery.parseJSON(text);
                if (dataObj.result) {
                    var data = dataObj.data[0];
                    document.getElementById("TabFrmID").value = data["TabFrmID"];
                    document.getElementById("TabFrmName").value = data["TabFrmName"];

                    var data_tpl = { list: dataObj.list, state: "load" };
                    //初始添加行的值
                    rowsCnt = dataObj.list.length;
                    //console.log(data_tpl);
                    var content = document.getElementById('t_list').innerHTML;
                    nodetpl.render(content, data_tpl, function (d) {
                        //console.log(d);
                        jQuery("#list").append(d);
                    });
                }
                else {
                    tips(dataObj.message);
                }
                layer.close(loadIndex);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                tips("Ajax错误，保存失败!");
                layer.close(loadIndex);
            }
        });
    }
    function myGetElementById(elementId) {
        return document.getElementById(elementId);
    }
    
    //保存表单信息编辑
    function system_saveData() {
        var dataName = document.getElementById("TabFrmName").value;
        if (dataName.length == 0)
        {
            tips("标签页框架名称不允许为空!");
            document.getElementById("TabFrmName").focus();
            return;
        }
        if (_dataId == undefined || _dataId == "") {
            _dataId = document.getElementById("TabFrmID").value;
            _isNew = "1";
        }
        var data_add = [];
        var data_del = [];
        var data_update = [];
        $("#list [id^='tr_']").each(function () {
            var state = $(this).attr("state");
            var oindex = $(this).attr("oindex");
            switch (state) {
                case "add":
                    data_add.push({
                        "TabID": myGetElementById("txt_TabID_" + oindex).value, "TabTitle": myGetElementById("txt_TabTitle_" + oindex).value,
                        "OrderID": myGetElementById("txt_OrderID_" + oindex).value, "TabUrl": myGetElementById("txt_TabUrl_" + oindex).value,
                        "TabHeight": myGetElementById("txt_TabHeight_" + oindex).value, "TabWidth": myGetElementById("txt_TabWidth_" + oindex).value,
                        "TabClose": myGetElementById("txt_TabClose_" + oindex).value == "on" ? 0 : 1
                    });
                    break;
                case "del":
                    data_del.push({ "TabID": myGetElementById("txt_TabID_" + oindex).value });
                    break;
                case "load":
                    //这里数据量较大时需要优化，针对有需要的修改才进行更新
                    data_update.push({
                        "TabID": myGetElementById("txt_TabID_" + oindex).value, "TabTitle": myGetElementById("txt_TabTitle_" + oindex).value,
                        "OrderID": myGetElementById("txt_OrderID_" + oindex).value, "TabUrl": myGetElementById("txt_TabUrl_" + oindex).value,
                        "TabHeight": myGetElementById("txt_TabHeight_" + oindex).value, "TabWidth": myGetElementById("txt_TabWidth_" + oindex).value,
                        "TabClose": myGetElementById("txt_TabClose_" + oindex).value == "on" ? 0 : 1
                    });
                    break;
            }
        });

        

        var data_add_str = "";
        if (data_add.length != "")
            data_add_str = JSON.stringify(data_add);
        console.log(data_add);
        var data_del_str = "";
        if (data_del.length != "")
            data_del_str = JSON.stringify(data_del);
        console.log(data_del);
        var data_update_str = "";
        if (data_update.length != "")
            data_update_str = JSON.stringify(data_update);
        console.log(data_update);


        //加载层-风格4
        loadIndex = layer.msg('正在保存，请稍后', { icon: 16, shade: [0.8, '#393D49'] });
        jQuery.ajax({
            url: "DataService/TabPageService.ashx?method=UpdateTabPageInfo",
            data: { dataAdd: data_add_str, dataDel: data_del_str, dataUpdate: data_update_str, TabFrmID: _dataId, TabFrmName: dataName },
            type: "post",
            success: function (text) {
                var dataObj = jQuery.parseJSON(text);
                if (dataObj.result == true) {
                    alert("保存成功");
                    window.opener.system_refresh();
                    window.close();
                } else {
                    tips(dataObj.message);
                }
                layer.close(loadIndex);                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                tips("Ajax错误，保存失败!");
                layer.close(loadIndex);
            }
        });
    }
    function tips(msg) {
        $.sticky({
            body: msg,
            position: 'top-mid',
            //useAnimateCss: true,
            closeable: false
        });
    }

    function winClose() {
        window.close();
    }

    function system_preview() {
        window.open(encodeURI("../SysExec/TabPage.aspx?TabFrmID=" + _dataId));
    }
</script>

<script id="t_list" type="text/template">

    <?for(var i=0; i<list.length; i++){?>
    <tr id="tr_<?=list[i].OrderID?>" oindex="<?=list[i].OrderID?>" class="form-item-sub" style="font-weight:bold;" state="<?=state?>" >
        <td style="display:none"><input id="txt_TabID_<?=list[i].OrderID?>" name="txt_TabID" value="<?=list[i].TabID?>" type="text" class="form-control" /></td>
        <td style="text-align:center"><input id="txt_OrderID_<?=list[i].OrderID?>" value="<?=list[i].OrderID?>" data-defaultValue="<?=list[i].OrderID?>" type="text" name="txt_OrderID" class="form-control" style="text-align:center"  /></td>
        <td style="text-align:center"><input id="txt_TabTitle_<?=list[i].OrderID?>"  value="<?=list[i].TabTitle?>" data-defaultValue="<?=list[i].TabTitle?>" name="txt_TabTitle" type="text" class="form-control" /></td>
        <td style="text-align:center"><input id="txt_TabUrl_<?=list[i].OrderID?>" value="<?=list[i].TabUrl?>" data-defaultValue="<?=list[i].TabUrl?>" type="text" name="txt_TabUrl" class="form-control" /></td>
        <td style="text-align:center"><input id="txt_TabHeight_<?=list[i].OrderID?>" value="<?=list[i].TabHeight?>" data-defaultValue="<?=list[i].TabHeight?>" type="text" name="txt_TabHeight" class="form-control" style="text-align:center"  /></td>
        <td style="text-align:center"><input id="txt_TabWidth_<?=list[i].OrderID?>" value="<?=list[i].TabWidth?>" data-defaultValue="<?=list[i].TabWidth?>" type="text" name="txt_TabWidth" class="form-control" style="text-align:center"  /></td>
        <td style="text-align:center"><input id="txt_TabClose_<?=list[i].OrderID?>" type="checkbox" name="txt_TabClose" class="form-control" style="text-align:center"  /></td>
        <td style="text-align:center"><span class="icon-remove icon-center" onclick="delRow('<?=list[i].OrderID?>');"></span></td>
     </tr>
     <?}?>
</script>