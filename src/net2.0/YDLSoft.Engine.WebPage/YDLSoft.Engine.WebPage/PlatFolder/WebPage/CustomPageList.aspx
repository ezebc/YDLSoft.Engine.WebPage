﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomPageList.aspx.cs" Inherits="YDLSoft.Engine.WebPage.PlatFolder.WebPage.CustomPageList" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="renderer" content="webkit" />
    <title>自定义页面-主界面</title>
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            border: 0;
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

    </style>
    <script src="../../scripts/jQuery/jquery.min.js"></script>
    <script src="../../scripts/miniui/boot.js"></script>
</head>
<body>
    <div class="mini-toolbar" borderstyle="border:0;">
        <a class="mini-button" iconcls="icon-add" plain="true" onclick="system_add()">增加</a>
        <a class="mini-button" iconcls="icon-edit" plain="true" onclick="system_edit()">编辑</a>
        <a class="mini-button" iconcls="icon-remove" plain="true" onclick="system_del()">删除</a>
        <span class="separator"></span>
        <a class="mini-button" iconcls="icon-reload" plain="true" onclick="system_refresh()">刷新</a>
        <a class="mini-button" iconcls="icon-reload" plain="true" onclick="system_preview()">预览</a>
        <a class="mini-button" iconcls="icon-help" plain="true" onclick="system_help()">使用说明</a>
    </div>
    <div class="mini-fit" style="overflow: hidden;">
        <div id="datagrid1" class="mini-datagrid" style="width: 100%; height: 100%;"
            allowresize="false" idfield="PageID" multiselect="true"
            pagesize="15" onrowdblclick="onRowDblClick">
            <div property="columns">
                <div type="checkcolumn"></div>
                <div field="PageID" width="100" headeralign="center">页面ID</div>
                <div field="PageTitle" headeralign="center">页面标题</div>
                <div field="CreateTime" width="50" headeralign="center" dateFormat="yyyy-MM-dd HH:mm">创建时间</div>
                <div field="UpdateTime" width="50" headeralign="center" dateFormat="yyyy-MM-dd HH:mm">更新时间</div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        mini.parse();
        var grid = mini.get("datagrid1");
        grid.setUrl("DataService/CustomPageService.ashx?method=GetCustomPageList");
        grid.load();

        function openCenter(url, name, width, height) {
            var str = "height=" + height + ",innerHeight=" + height + ",width=" + width + ",innerWidth=" + width;
            if (window.screen) {
                var ah = screen.availHeight - 30;
                var aw = screen.availWidth - 10;
                var xc = (aw - width) / 2;
                var yc = (ah - height) / 2;
                str += ",left=" + xc + ",screenX=" + xc + ",top=" + yc + ",screenY=" + yc;
                str += ",resizable=yes,scrollbars=yes,directories=no,status=no,toolbar=no,menubar=no,location=no";
            }
            return window.open(url, name, str);
        }
        //添加
        function system_add() {
            var urlStr = "CustomPageInfo.aspx";
            //window.open(encodeURI(urlStr));
            openCenter(urlStr, "添加", 800, 600);
        }
        
        //修改
        function system_edit() {
            var row = grid.getSelected();
            if (row) {
                window.open(encodeURI("CustomPageInfo.aspx?PageID=" + row.PageID));
            }
        }
        //双击行编辑
        function onRowDblClick(e) {
            var row = e.row;
            if (row) {
                window.open(encodeURI("CustomPageInfo.aspx?PageID=" + row.PageID));
            }
        }
        //查询
        function system_search() {
            grid.load()
        }
        function system_preview() {
            var row = grid.getSelected();
            if (row) {
                window.open(encodeURI("../SysExec/CustomPage.aspx?PageID=" + row.PageID));
            } else {
                alert("请选择一条记录");
            }
        }
        //刷新
        function system_refresh() {
            grid.reload();
        }
        //删除一条表单信息
        function system_del() {
            var rows = grid.getSelecteds();
            if (rows.length > 0) {
                if (confirm("确定删除选中记录？")) {
                    var ids = [];
                    for (var i = 0, l = rows.length; i < l; i++) {
                        var r = rows[i];
                        ids.push(r.PageID);
                    }
                    var id = ids.join(',');
                    grid.loading("操作中，请稍后......");
                    $.ajax({
                        url: "DataService/CustomPageService.ashx?method=DelCustomPageInfo&PageID=" + id,
                        success: function (text) {
                            var dataObj = jQuery.parseJSON(text);
                            if (dataObj.result == true) {
                                grid.unmask();
                                grid.reload();
                            }
                            else {
                                alert(dataObj.message);
                                grid.unmask();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Ajax错误，删除失败!" + errorThrown);
                            grid.unmask();
                        }
                    });
                }
            } else {
                alert("请选中一条记录");
            }
        }
    </script>
</body>
</html>