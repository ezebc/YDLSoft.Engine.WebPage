﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.SessionState;
using YDLSoft.DataBase.DataAcess;

namespace YDLSoft.Engine.WebPage.PlatFolder.WebPage.DataService
{
    /// <summary>
    /// TabPageService 的摘要说明
    /// </summary>
    public class TabPageService : IHttpHandler, IRequiresSessionState
    {
        Hashtable result = new Hashtable();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Request.ContentEncoding = Encoding.GetEncoding("UTF-8");
            /////////////////
            String methodName = context.Request["method"];
            if (string.IsNullOrEmpty(methodName))
            {
                result["result"] = false;
                result["message"] = "缺少参数：method";
                String json = JsonConvert.SerializeObject(result);
                context.Response.Clear();
                context.Response.Write(json);
            }
            else
            {
                Type type = this.GetType();
                MethodInfo method = type.GetMethod(methodName);
                if (method == null)
                {
                    result["result"] = false;
                    result["message"] = "没有找到方法：" + methodName;
                    String json = JsonConvert.SerializeObject(result);
                    context.Response.Clear();
                    context.Response.Write(json);
                    context.Response.End();
                }
                try
                {
                    if (BeforeInvoke(methodName))
                        method.Invoke(this, null);
                }
                catch (Exception ex)
                {
                    result["methodName"] = methodName;
                    result["result"] = false;
                    result["message"] = ex.Message;
                    result["stackTrace"] = ex.StackTrace;
                    String json = JsonConvert.SerializeObject(result);
                    context.Response.Clear();
                    context.Response.Write(json);
                }
                finally
                {
                    AfterInvoke(methodName);
                }
            }
        }

        //权限管理
        protected bool BeforeInvoke(String methodName)
        {
            if (HttpContext.Current.Session["loginname"] == null)
            {
                result["result"] = false;
                result["message"] = "用户未登录或已超时，请重新登录！";
                result["statu"] = "-1";
                String json = JsonConvert.SerializeObject(result);

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(json);
                //HttpContext.Current.Response.End();
                return false;
            }
            return true;

        }
        //日志管理
        protected void AfterInvoke(String methodName)
        {

        }

        /// <summary>
        /// 获得自定义页面列表
        /// </summary>
        /// <example>
        /// DataService/TabPageService.ashx?method=GetTabPageList&sqlQueryCondition=
        /// </example>
        public void GetTabPageList()
        {
            string json = "";
            Hashtable hashtable = new Hashtable();
            //分页
            int pageIndex = 0;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["pageIndex"]))
                pageIndex = Convert.ToInt32(HttpContext.Current.Request["pageIndex"]);
            int pageSize = 10;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["pageSize"]))
                pageSize = Convert.ToInt32(HttpContext.Current.Request["pageSize"]);

            //查询条件
            string strWhere = "";
            //搜索查询过滤
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["SqlQueryCondition"]))
            {
                strWhere += HttpContext.Current.Request["SqlQueryCondition"].ToString();
            }
            //客户端初始查询条件
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["cSqlQueryCondition"]))
            {
                if (strWhere != "")
                    strWhere += " and ";
                strWhere += HttpContext.Current.Request["cSqlQueryCondition"].ToString();
            }
            if (strWhere == null || strWhere == "")
                strWhere = "1=1";
            try
            {
                string sqlStr = "select * from plat_engine_webpage_tabfrm where " + strWhere;
                int totalCnt = 0;
                DataTable dt = SystemDB.SplitPage(sqlStr, pageIndex + 1, pageSize, out totalCnt);
                hashtable["total"] = totalCnt;
                hashtable["data"] = dt;

                json = JsonConvert.SerializeObject(hashtable);
                dt.Dispose();
            }
            catch (Exception ex)
            {
                hashtable["result"] = false;
                hashtable["message"] = ex.Message;
            }
            hashtable = null;
            HttpContext.Current.Response.Write(json);
        }

        /// <summary>
        /// 获得标签页配置列表
        /// </summary>
        /// <example>
        /// DataService/TabPageService.ashx?method=GetTabsAllList&TabFrmID=
        /// </example>
        public void GetTabsAllList()
        {
            string json = "";
            Hashtable hashtable = new Hashtable();
            string dataId = HttpContext.Current.Request["TabFrmID"];
            if (string.IsNullOrEmpty(dataId))
            {
                hashtable["result"] = false;
                hashtable["message"] = "缺少参数：TabFrmID";
                json = JsonConvert.SerializeObject(hashtable);
                hashtable = null;

                HttpContext.Current.Response.Write(json);
                return;
            }
            try
            {
                string sqlStr = "select * from plat_engine_webpage_tabfrm_tabs where RefTabFrmID='"+ dataId + "' order by OrderID ";
                DataTable dt = SystemDB.ExecuteTable(sqlStr);
                hashtable["result"] = true;
                hashtable["data"] = dt;
                json = JsonConvert.SerializeObject(hashtable);
                dt.Dispose();
            }
            catch (Exception ex)
            {
                hashtable["result"] = false;
                hashtable["message"] = ex.Message;
            }
            hashtable = null;
            HttpContext.Current.Response.Write(json);
        }

        /// <summary>
        /// 获得标签页框架信息
        /// </summary>
        /// <example>
        /// DataService/TabPageService.ashx?method=GetTabPageInfo&TabFrmID=
        /// </example>
        public void GetTabPageInfo()
        {
            String json = "";
            Hashtable hashtable = new Hashtable();
            hashtable["dataType"] = "info";
            string dataId = HttpContext.Current.Request["TabFrmID"];
            if (string.IsNullOrEmpty(dataId))
            {
                hashtable["result"] = false;
                hashtable["message"] = "缺少参数：TabFrmID";
                json = JsonConvert.SerializeObject(hashtable);
                hashtable = null;

                HttpContext.Current.Response.Write(json);
                return;
            }
            string sqlStr = "select * from plat_engine_webpage_tabfrm where TabFrmID='" + dataId + "' ";
            DataTable dt = SystemDB.ExecuteTable(sqlStr);
            if (null != dt && dt.Rows.Count > 0)
            {
                hashtable["result"] = true;
                hashtable["message"] = "ok";
                hashtable["data"] = dt;
                sqlStr = "select * from plat_engine_webpage_tabfrm_tabs where RefTabFrmID='" + dataId + "' order by OrderID ";
                DataTable dt_list = SystemDB.ExecuteTable(sqlStr);
                hashtable["list"] = dt_list;
            }
            else
            {
                hashtable["result"] = false;
                hashtable["message"] = "没有找到相关数据";
                hashtable["data"] = null;
            }
            IsoDateTimeConverter timeConverter = new IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式     
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            json = JsonConvert.SerializeObject(hashtable, timeConverter);
            dt.Dispose();
            timeConverter = null;
            hashtable = null;

            HttpContext.Current.Response.Write(json);
        }


        /// <summary>
        /// 更新数据信息
        /// </summary>
        /// <example>
        /// DataService/TabPageService.ashx?method=UpdateTabPageInfo&PageID&data=
        /// </example>
        /// <remarks>
        /// 内容项的添加，删除，修改数据进行了区分
        /// 提高了执行性能,无关数据不进行操作
        /// DataAdd参数格式{"DataID":"","DataName":""}
        /// DataDel参数格式[{"ItemID":"","":""}]
        /// </remarks>
        public void UpdateTabPageInfo()
        {
            String json = "";
            Hashtable hashtable = new Hashtable();
            //是否新建模式
            string isNew = "0";
            if (string.IsNullOrEmpty(HttpContext.Current.Request["TabFrmID"]))
            {
                isNew = "1";
            }
            string tabFrmName = HttpContext.Current.Request["TabFrmName"];
            if (string.IsNullOrEmpty(tabFrmName))
            {
                hashtable["result"] = false;
                hashtable["message"] = "标签页框架名称不允许为空,请修改名称后保存!";
                json = JsonConvert.SerializeObject(hashtable);
                hashtable = null;

                HttpContext.Current.Response.Write(json);
                return;
            }
            //格式{"DataID":"","DataName":""}
            string dataAdd = HttpContext.Current.Request["dataAdd"];
            dataAdd = HttpUtility.UrlDecode(dataAdd, Encoding.UTF8);
            string dataDel = HttpContext.Current.Request["dataDel"];
            dataDel = HttpUtility.UrlDecode(dataDel, Encoding.UTF8);
            string dataUpdate = HttpContext.Current.Request["dataUpdate"];
            dataUpdate = HttpUtility.UrlDecode(dataUpdate, Encoding.UTF8);
            DbConnection dbConn = null;
            DbTransaction dbTransaction = null;
            try
            {
                dbConn = SystemDB.CreateConnection();
                dbConn.Open();
                dbTransaction = dbConn.BeginTransaction();
                Hashtable para = new Hashtable();
                para["TabFrmName"] = tabFrmName;
                List<Hashtable> dataAddList = null;
                if (null != dataAdd && dataAdd != "")
                {
                    dataAddList = JsonConvert.DeserializeObject<List<Hashtable>>(dataAdd);
                }
                List<Hashtable> dataDelList = null;
                if (null != dataDel && dataDel != "")
                {
                    dataDelList = JsonConvert.DeserializeObject<List<Hashtable>>(dataDel);
                }
                List<Hashtable> dataUpdateList = null;
                if (null != dataUpdate && dataUpdate != "")
                {
                    dataUpdateList = JsonConvert.DeserializeObject<List<Hashtable>>(dataUpdate);
                }
                string sqlStr = "";
                if (isNew == "1")
                {                    
                    sqlStr = @"select count(1) as cnt from plat_engine_webpage_tabfrm where TabFrmName='" + para["TabFrmName"] + "' ";
                    int result = (int)SystemDB.ExecuteScalar(sqlStr);
                    if (result > 0)
                    {
                        hashtable["result"] = false;
                        hashtable["message"] = "标签页框架名称已存在,请修改名称后保存!";
                        json = JsonConvert.SerializeObject(hashtable);
                        hashtable = null;

                        HttpContext.Current.Response.Write(json);
                        return;
                    }
                    para["TabFrmID"] = Guid.NewGuid().ToString();
                    para["CreateTime"] = DateTime.Now.ToString();
                    para["UpdateTime"] = para["CreateTime"];
                    sqlStr = "insert into plat_engine_webpage_tabfrm (TabFrmID,TabFrmName,CreateTime,UpdateTime) values(@TabFrmID, @TabFrmName,@CreateTime,@UpdateTime)";
                    SystemDB.ExecuteNonQuery(sqlStr, para, dbTransaction);
                    if (dataAddList != null)
                    {
                        for (int i = 0, len = dataAddList.Count; i < len; i++)
                        {
                            sqlStr = "insert into plat_engine_webpage_tabfrm_tabs (TabID,TabTitle,TabUrl,TabHeight,TabWidth,TabClose,OrderID,RefTabFrmID) ";
                            sqlStr += "values('" + Guid.NewGuid().ToString() + "', '" + dataAddList[i]["TabTitle"] + "','" + dataAddList[i]["TabUrl"] + "'";
                            sqlStr += ",'" + dataAddList[i]["TabHeight"] + "','" + dataAddList[i]["TabWidth"] + "','" + dataAddList[i]["TabClose"] + "'";
                            sqlStr += ",'" + dataAddList[i]["OrderID"] + "','" + para["TabFrmID"] + "')";
                            SystemDB.ExecuteNonQuery(sqlStr, dbTransaction);
                        }
                    }
                }
                else
                {
                    para["TabFrmID"] = HttpContext.Current.Request["TabFrmID"];
                    sqlStr = @"select count(1) as cnt from plat_engine_webpage_tabfrm where TabFrmName='" + para["TabFrmName"] + "' and TabFrmID != '" + para["TabFrmID"] + "' ";
                    int result = (int)SystemDB.ExecuteScalar(sqlStr);
                    if (result > 0)
                    {
                        hashtable["result"] = false;
                        hashtable["message"] = "标签页框架名称已存在,请修改名称后保存!";
                        json = JsonConvert.SerializeObject(hashtable);
                        hashtable = null;

                        HttpContext.Current.Response.Write(json);
                        return;
                    }
                    para["UpdateTime"] = DateTime.Now.ToString();
                    sqlStr = @"update plat_engine_webpage_tabfrm set TabFrmName=@TabFrmName,UpdateTime=@UpdateTime where TabFrmID=@TabFrmID ";
                    SystemDB.ExecuteNonQuery(sqlStr, para, dbTransaction);
                    //内容添加
                    if (null != dataAddList)
                    {
                        for (int i = 0, len = dataAddList.Count; i < len; i++)
                        {
                            sqlStr = "insert into plat_engine_webpage_tabfrm_tabs (TabID,TabTitle,TabUrl,TabHeight,TabWidth,TabClose,OrderID,RefTabFrmID) ";
                            sqlStr += "values('" + Guid.NewGuid().ToString() + "', '" + dataAddList[i]["TabTitle"] + "','" + dataAddList[i]["TabUrl"] + "'";
                            sqlStr += ",'" + dataAddList[i]["TabHeight"] + "','" + dataAddList[i]["TabWidth"] + "','" + dataAddList[i]["TabClose"] + "'";
                            sqlStr += ",'" + dataAddList[i]["OrderID"] + "','" + para["TabFrmID"] + "')";
                            SystemDB.ExecuteNonQuery(sqlStr, dbTransaction);
                        }
                    }
                    if (null != dataDelList)
                    {
                        //内容删除
                        for (int i = 0, len = dataDelList.Count; i < len; i++)
                        {
                            sqlStr = "delete from plat_engine_webpage_tabfrm_tabs where TabID='" + dataDelList[i]["TabID"] + "' ";
                            SystemDB.ExecuteNonQuery(sqlStr, dbTransaction);
                        }
                    }
                    if (null != dataUpdateList)
                    {
                        //内容修改
                        for (int i = 0, len = dataUpdateList.Count; i < len; i++)
                        {
                            sqlStr = "update plat_engine_webpage_tabfrm_tabs set TabTitle='" + dataUpdateList[i]["TabTitle"] + "',";
                            sqlStr += "TabUrl='" + dataUpdateList[i]["TabUrl"] + "',";
                            sqlStr += "TabHeight='" + dataUpdateList[i]["TabHeight"] + "',";
                            sqlStr += "TabWidth='" + dataUpdateList[i]["TabWidth"] + "',";
                            sqlStr += "TabClose='" + dataUpdateList[i]["TabClose"] + "',";
                            sqlStr += "OrderID='" + dataUpdateList[i]["OrderID"] + "' ";
                            sqlStr += "where TabID='" + dataUpdateList[i]["TabID"] + "'";

                            SystemDB.ExecuteNonQuery(sqlStr, dbTransaction);
                        }
                    }
                }
                dbTransaction.Commit();

                hashtable["result"] = true;
                hashtable["message"] = "ok";
            }
            catch (Exception ex)
            {
                hashtable["result"] = false;
                hashtable["message"] = ex.Message;
                dbTransaction.Rollback();
            }
            finally
            {
                dbConn.Close();

            }
            json = JsonConvert.SerializeObject(hashtable);
            hashtable = null;

            HttpContext.Current.Response.Write(json);
        }

        /// <summary>
        /// 更新表单列表SQL
        /// </summary>
        /// <example>
        /// DataService/TabPageService.ashx?method=DelTabPageInfo&DictID=
        /// </example>
        public void DelTabPageInfo()
        {
            String json = "";
            Hashtable hashtable = new Hashtable();
            string dataID = HttpContext.Current.Request["TabFrmID"];
            if (string.IsNullOrEmpty(dataID))
            {
                hashtable["result"] = false;
                hashtable["message"] = "缺少参数：TabFrmID";
            }
            else
            {
                try
                {
                    string[] arr_data = dataID.Split(',');
                    string sqlStr = "";
                    if (arr_data.Length == 1)
                    {
                        sqlStr = "delete from plat_engine_webpage_tabfrm where TabFrmID=@DataID \n";
                        sqlStr += "delete from plat_engine_webpage_tabfrm_tabs where RefTabFrmID=@DataID \n";
                        sqlStr = sqlStr.Replace("@DataID", "'" + dataID.Replace("'", "''") + "'");
                    }
                    else
                    {
                        sqlStr = "delete from plat_engine_webpage_tabfrm where TabFrmID in (@DataID) \n";
                        sqlStr += "delete from plat_engine_webpage_tabfrm_tabs where RefTabFrmID in (@DataID) \n";
                        sqlStr = sqlStr.Replace("@DataID", "'" + string.Join("','", arr_data) + "'");
                    }
                    int cnt = 0;
                    cnt = SystemDB.ExecuteNonQuery(sqlStr);

                    if (cnt > 0)
                    {
                        hashtable["result"] = true;
                        hashtable["message"] = "ok";
                    }
                    else
                    {
                        hashtable["result"] = false;
                        hashtable["message"] = "没有找到相关数据";
                        hashtable["dataId"] = "";
                    }
                }
                catch (Exception ex)
                {
                    hashtable["result"] = false;
                    hashtable["message"] = ex.Message;
                }
            }

            json = JsonConvert.SerializeObject(hashtable);
            hashtable = null;

            HttpContext.Current.Response.Write(json);
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}