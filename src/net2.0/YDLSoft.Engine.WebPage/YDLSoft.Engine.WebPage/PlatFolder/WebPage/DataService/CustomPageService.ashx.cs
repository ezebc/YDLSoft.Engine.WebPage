﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.SessionState;
using YDLSoft.DataBase.DataAcess;


namespace YDLSoft.Engine.WebPage.PlatFolder.WebPage.DataService
{
    /// <summary>
    /// CustomPageService 的摘要说明
    /// </summary>
    public class CustomPageService : IHttpHandler, IRequiresSessionState
    {
        Hashtable result = new Hashtable();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Request.ContentEncoding = Encoding.GetEncoding("UTF-8");
            /////////////////
            String methodName = context.Request["method"];
            if (string.IsNullOrEmpty(methodName))
            {
                result["result"] = false;
                result["message"] = "缺少参数：method";
                String json = JsonConvert.SerializeObject(result);
                context.Response.Clear();
                context.Response.Write(json);
            }
            else
            {
                Type type = this.GetType();
                MethodInfo method = type.GetMethod(methodName);
                if (method == null)
                {
                    result["result"] = false;
                    result["message"] = "没有找到方法：" + methodName;
                    String json = JsonConvert.SerializeObject(result);
                    context.Response.Clear();
                    context.Response.Write(json);
                    context.Response.End();
                }
                try
                {
                    if (BeforeInvoke(methodName))
                        method.Invoke(this, null);
                }
                catch (Exception ex)
                {
                    result["methodName"] = methodName;
                    result["result"] = false;
                    result["message"] = ex.Message;
                    result["stackTrace"] = ex.StackTrace;
                    String json = JsonConvert.SerializeObject(result);
                    context.Response.Clear();
                    context.Response.Write(json);
                }
                finally
                {
                    AfterInvoke(methodName);
                }
            }
        }

        //权限管理
        protected bool BeforeInvoke(String methodName)
        {
            if (HttpContext.Current.Session["loginname"] == null)
            {
                result["result"] = false;
                result["message"] = "用户未登录或已超时，请重新登录！";
                result["statu"] = "-1";
                String json = JsonConvert.SerializeObject(result);

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(json);
                //HttpContext.Current.Response.End();
                return false;
            }
            return true;

        }
        //日志管理
        protected void AfterInvoke(String methodName)
        {

        }

        /// <summary>
        /// 获得自定义页面列表
        /// </summary>
        /// <example>
        /// DataService/CustomPageService.ashx?method=GetCustomPageList&sqlQueryCondition=
        /// </example>
        public void GetCustomPageList()
        {
            string json = "";
            Hashtable hashtable = new Hashtable();
            //分页
            int pageIndex = 0;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["pageIndex"]))
                pageIndex = Convert.ToInt32(HttpContext.Current.Request["pageIndex"]);
            int pageSize = 10;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["pageSize"]))
                pageSize = Convert.ToInt32(HttpContext.Current.Request["pageSize"]);

            //查询条件
            string strWhere = "";
            //搜索查询过滤
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["SqlQueryCondition"]))
            {
                strWhere += HttpContext.Current.Request["SqlQueryCondition"].ToString();
            }
            //客户端初始查询条件
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["cSqlQueryCondition"]))
            {
                if (strWhere != "")
                    strWhere += " and ";
                strWhere += HttpContext.Current.Request["cSqlQueryCondition"].ToString();
            }
            if (strWhere == null || strWhere == "")
                strWhere = "1=1";
            try
            {
                string sqlStr = "select PageID,PageTitle,CreateTime,UpdateTime from plat_engine_webpage_custom_page where " + strWhere;
                int totalCnt = 0;
                DataTable dt = SystemDB.SplitPage(sqlStr, pageIndex + 1, pageSize, out totalCnt);
                hashtable["total"] = totalCnt;
                hashtable["data"] = dt;

                json = JsonConvert.SerializeObject(hashtable);
                dt.Dispose();
            }
            catch (Exception ex)
            {
                hashtable["result"] = false;
                hashtable["message"] = ex.Message;
            }
            hashtable = null;
            HttpContext.Current.Response.Write(json);
        }

        /// <summary>
        /// 获得表单模型信息
        /// </summary>
        /// <example>
        /// DataService/CustomPageService.ashx?method=GetCustomPageInfo&PageID=
        /// </example>
        public void GetCustomPageInfo()
        {
            String json = "";
            Hashtable hashtable = new Hashtable();
            hashtable["dataType"] = "info";
            string dataId = HttpContext.Current.Request["PageID"];
            if (string.IsNullOrEmpty(dataId))
            {
                hashtable["result"] = false;
                hashtable["message"] = "缺少参数：PageID";
                json = JsonConvert.SerializeObject(hashtable);
                hashtable = null;

                HttpContext.Current.Response.Write(json);
                return;
            }
            string sqlStr = "select * from plat_engine_webpage_custom_page where PageID='" + dataId + "' ";
            DataTable dt = SystemDB.ExecuteTable(sqlStr);
            if (null != dt && dt.Rows.Count > 0)
            {
                hashtable["result"] = true;
                hashtable["message"] = "ok";
                hashtable["data"] = dt;
            }
            else
            {
                hashtable["result"] = false;
                hashtable["message"] = "没有找到相关数据";
                hashtable["data"] = null;
            }
            IsoDateTimeConverter timeConverter = new IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式     
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            json = JsonConvert.SerializeObject(hashtable, timeConverter);
            dt.Dispose();
            timeConverter = null;
            hashtable = null;

            HttpContext.Current.Response.Write(json);
        }


        /// <summary>
        /// 更新数据信息
        /// </summary>
        /// <example>
        /// DataService/CustomPageService.ashx?method=UpdateCustomPageInfo&PageID&data=
        /// </example>
        /// <remarks>
        /// 内容项的添加，删除，修改数据进行了区分
        /// 提高了执行性能,无关数据不进行操作
        /// DataAdd参数格式{"DataID":"","DataName":""}
        /// DataDel参数格式[{"ItemID":"","":""}]
        /// </remarks>
        public void UpdateCustomPageInfo()
        {
            String json = "";
            Hashtable hashtable = new Hashtable();

            //格式{"DataID":"","DataName":""}
            string data = HttpContext.Current.Request["data"];
            data = HttpUtility.UrlDecode(data, Encoding.UTF8);
            Hashtable para = null;
            if (null != data)
            {
                para = JsonConvert.DeserializeObject<Hashtable>(data);
            }
            //是否新建模式
            string isNew = "0";
            if (string.IsNullOrEmpty(para["PageID"].ToString()))
            {
                isNew = "1";
            }
            if (string.IsNullOrEmpty(para["PageTitle"].ToString()))
            {
                hashtable["result"] = false;
                hashtable["message"] = "页面名称不允许为空,请修改名称后保存!";
                json = JsonConvert.SerializeObject(hashtable);
                hashtable = null;

                HttpContext.Current.Response.Write(json);
                return;
            }
            
            try
            {                
                string sqlStr = "";
                if (isNew == "1")
                {
                    para["PageID"] = Guid.NewGuid().ToString();
                    sqlStr = @"select count(1) as cnt from plat_engine_webpage_custom_page where PageTitle='" + para["PageTitle"] + "' ";
                    int result = (int)SystemDB.ExecuteScalar(sqlStr);
                    if (result > 0)
                    {
                        hashtable["result"] = false;
                        hashtable["message"] = "页面名称已存在,请修改名称后保存!";
                        json = JsonConvert.SerializeObject(hashtable);
                        hashtable = null;

                        HttpContext.Current.Response.Write(json);
                        return;
                    }
                    para["CreateTime"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    para["UpdateTime"] = para["CreateTime"];
                    sqlStr = "insert into plat_engine_webpage_custom_page(PageID,PageTitle,CreateTime,UpdateTime,PageHtml) values(@PageID, @PageTitle,@CreateTime,@UpdateTime,@PageHtml)";                  
                }
                else
                {
                    sqlStr = @"select count(1) as cnt from plat_engine_webpage_custom_page where PageTitle='" + para["PageTitle"] + "' and PageID != '" + para["PageID"] + "' ";
                    int result = (int)SystemDB.ExecuteScalar(sqlStr);
                    if (result > 0)
                    {
                        hashtable["result"] = false;
                        hashtable["message"] = "页面名称已存在,请修改名称后保存!";
                        json = JsonConvert.SerializeObject(hashtable);
                        hashtable = null;

                        HttpContext.Current.Response.Write(json);
                        return;
                    }
                    para["UpdateTime"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    sqlStr = @"update plat_engine_webpage_custom_page set PageTitle=@PageTitle,UpdateTime=@UpdateTime,PageHtml=@PageHtml where PageID=@PageID ";                    
                }

                int cnt = 0;
                cnt = SystemDB.ExecuteNonQuery(sqlStr, para);
                sqlStr = null;
                if (cnt > 0)
                {
                    hashtable["result"] = true;
                    hashtable["message"] = "ok";
                }
                else
                {
                    hashtable["result"] = false;
                    hashtable["message"] = "没有找到相关数据";
                }
            }
            catch (Exception ex)
            {
                hashtable["result"] = false;
                hashtable["message"] = ex.Message;
                
            }
            json = JsonConvert.SerializeObject(hashtable);
            hashtable = null;

            HttpContext.Current.Response.Write(json);
        }

        /// <summary>
        /// 更新表单列表SQL
        /// </summary>
        /// <example>
        /// DataService/CustomPageService.ashx?method=DelCustomPageInfo&DictID=
        /// </example>
        public void DelCustomPageInfo()
        {
            String json = "";
            Hashtable hashtable = new Hashtable();
            string dataID = HttpContext.Current.Request["PageID"];
            if (string.IsNullOrEmpty(dataID))
            {
                hashtable["result"] = false;
                hashtable["message"] = "缺少参数：PageID";
            }
            else
            {
                try
                {
                    string[] arr_data = dataID.Split(',');
                    string sqlStr = "";
                    if (arr_data.Length == 1)
                    {
                        sqlStr = "delete from plat_engine_webpage_custom_page where PageID=@DataID \n";
                        sqlStr = sqlStr.Replace("@DataID", "'" + dataID.Replace("'", "''") + "'");
                    }
                    else
                    {
                        sqlStr = "delete from plat_engine_webpage_custom_page where PageID in (@DataID) \n";
                        sqlStr = sqlStr.Replace("@DataID", "'" + string.Join("','", arr_data) + "'");
                    }
                    int cnt = 0;
                    cnt = SystemDB.ExecuteNonQuery(sqlStr);

                    if (cnt > 0)
                    {
                        hashtable["result"] = true;
                        hashtable["message"] = "ok";
                    }
                    else
                    {
                        hashtable["result"] = false;
                        hashtable["message"] = "没有找到相关数据";
                        hashtable["dataId"] = "";
                    }
                }
                catch (Exception ex)
                {
                    hashtable["result"] = false;
                    hashtable["message"] = ex.Message;
                }
            }

            json = JsonConvert.SerializeObject(hashtable);
            hashtable = null;

            HttpContext.Current.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        
    }
}