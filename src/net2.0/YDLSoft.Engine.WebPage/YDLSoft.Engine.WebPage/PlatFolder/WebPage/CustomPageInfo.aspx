﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomPageInfo.aspx.cs" Inherits="YDLSoft.Engine.WebPage.PlatFolder.WebPage.CustomPageInfo" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>数据源信息</title>
    <link href="../../themes/default/skin.css" rel="stylesheet" />
    <link href="../../Scripts/sticky/jquery.sticky.min.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src="../../Scripts/common/html5shiv.min.js"></script>
    <![endif]-->
    <link href="../../Scripts/codemirror/lib/codemirror.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../Scripts/codemirror/addon/hint/show-hint.css" />
    
    <script src="../../Scripts/codemirror/lib/codemirror.js"></script>
    <script src="../../Scripts/codemirror/addon/selection/selection-pointer.js"></script>
    <script src="../../Scripts/codemirror/mode/xml/xml.js"></script>
    <script src="../../Scripts/codemirror/mode/javascript/javascript.js"></script>
    <script src="../../Scripts/codemirror/mode/css/css.js"></script>
    <script src="../../Scripts/codemirror/mode/vbscript/vbscript.js"></script>
    <script src="../../Scripts/codemirror/mode/htmlmixed/htmlmixed.js"></script>


    <style type="text/css">
        .CodeMirror {border-top: 1px solid #888; border-bottom: 1px solid #888;}
    </style>
    <script src="../../Scripts/jQuery/jquery.min.js"></script>
    <script type="text/javascript">
        var editor_pageHtml = null;
        jQuery(function ($) {
            editor_pageHtml = CodeMirror.fromTextArea(document.getElementById("PageHtml"), {
                mode: "text/html",
                lineWrapping: true,
                autoMatchParens: true,
                lineNumbers: true
            });
            editor_pageHtml.setSize('height', $(window).height() - 185);
            //editor_pageHtml.setSize('auto', 'auto');
        })
    </script>
</head>
<body>
    <div id="head" >
        <div style="text-align:right;padding-right:20px;">
            <input id="btn_save" type="button" value="保存" name="btn_save" onclick="system_saveData();" />
            <input id="btn_preview" style="display:none" type="button" value="预览" name="btn_save" onclick="system_preview();" />
        </div>
    </div>
    <div id="content">
		<div class="con">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table">
				<tr>
					<td colspan="4" class="t">自定义页面</td>
				</tr>
				<tr style="display:none">
					<td class="l u">页面ID</td>
					<td class="l" colspan="3">
                        <input id="PageID" name="PageID" class="form-control" readonly="readonly" value="" /> 
                    </td>					
				</tr>
				<tr>
                    <td class="l u" style="width:15%;">页面名称</td>
                    <td class="l" style="width:35%;"><input id="PageTitle" name="PageTitle" class="form-control require" type="text" value="" /></td>
				</tr>                
                <tr>
                    <td class="l" colspan="4">
                        <textarea id="PageHtml" name="PageHtml" class="form-textarea"  rows="3"></textarea>
                    </td>
                </tr>                
			</table>
		</div>
	</div>
   
</body>
</html>
<script src="../../Scripts/jQuery/jquery.min.js"></script>
<script src="../../Scripts/sticky/jquery.sticky.min.js"></script>
<script src="../../../Scripts/common/ydlsoft.js"></script>
<script src="../../Scripts/layer/layer.js"></script>
<script type="text/javascript">
    var ydlSoft = new YDLSoft();
    var _dataId = ydlSoft.GetQueryString("PageID");
    //默认是修改
    var _isNew = "0";
    var loadIndex = -1;
    $(document).ready(function(){        
        if (_dataId) {
            _isNew = "0";
            document.getElementById("btn_preview").style.display = "";
            loadDataInfo(_dataId);
        } else {
            _isNew = "1";
            document.getElementById("PageTitle").focus();
            var temp = '<!DOCTYPE html>\n<html>\n<head>\n' +
            '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>\n' +
            '    <title></title>\n</head>\n<body>\n    \n</body>\n</html>';
            editor_pageHtml.setValue(temp);
        }
    });
    $(document).keydown(function (e) {
        //ctrl+s
        if (e.ctrlKey && event.keyCode == 83) {
            document.getElementById("PageTitle").focus();
            system_saveData();
            return false;
        }
    });

    //加载表单信息编辑
    function loadDataInfo(dataId)
    {
        //加载层-风格4
        loadIndex = layer.msg('正在加载，请稍后', { icon: 16, shade: [0.8, '#393D49'] });
        jQuery.ajax({
            url: "DataService/CustomPageService.ashx?method=GetCustomPageInfo&PageID=" + dataId,
            type: "post",
            success: function (text) {
                var dataObj = jQuery.parseJSON(text);
                if (dataObj.result) {
                    var data = dataObj.data[0];
                    document.getElementById("PageID").value = data["PageID"];
                    document.getElementById("PageTitle").value = data["PageTitle"];
                    document.getElementById("PageHtml").value = data["PageHtml"];
                    editor_pageHtml.setValue(data["PageHtml"]);
                }
                else {
                    tips(dataObj.message);
                }
                layer.close(loadIndex);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                tips("Ajax错误，保存失败!");
                layer.close(loadIndex);
            }
        });
    }
    
    //保存表单信息编辑
    function system_saveData() {
        var dataName = document.getElementById("PageTitle").value;
        if (dataName.length == 0)
        {
            tips("页面名称不允许为空!");
            document.getElementById("PageTitle").focus();
            return;
        }
        if (_dataId == undefined || _dataId == "") {
            _dataId = document.getElementById("PageID").value;
            _isNew = "1";
        }
        
        var objData = {};        
        objData["PageID"] = document.getElementById("PageID").value;
        objData["PageTitle"] = document.getElementById("PageTitle").value;
        document.getElementById("PageHtml").value = editor_pageHtml.getValue();
        objData["PageHtml"] = document.getElementById("PageHtml").value;

        var dataJson = JSON.stringify(objData);
        //加载层-风格4
        loadIndex = layer.msg('正在保存，请稍后', { icon: 16, shade: [0.8, '#393D49'] });
        jQuery.ajax({
            url: "DataService/CustomPageService.ashx?method=UpdateCustomPageInfo",
            data: { data: dataJson },
            type: "post",
            success: function (text) {
                var dataObj = jQuery.parseJSON(text);
                if (dataObj.result == true) {
                    alert("保存成功");
                    window.opener.system_refresh();
                    window.close();
                } else {
                    tips(dataObj.message);
                }
                layer.close(loadIndex);
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                tips("Ajax错误，保存失败!");
                layer.close(loadIndex);
            }
        });
    }
    function tips(msg) {
        $.sticky({
            body: msg,
            position: 'top-mid',
            //useAnimateCss: true,
            closeable: false
        });
    }

    function winClose() {
        window.close();
    }

    $(window).resize(function () {
        editor_pageHtml.setSize('height', $(window).height() - 185);
    });

    function system_preview() {
        window.open(encodeURI("../SysExec/CustomPage.aspx?PageID=" + _dataId));
    }
</script>