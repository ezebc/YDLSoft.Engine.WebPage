﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TabPage.aspx.cs" Inherits="YDLSoft.Engine.WebPage.PlatFolder.SysExec.TabPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title></title>
    <link href="../../Scripts/bootstrap/v3.3.5/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../Scripts/bootstrap/tabs/bootstrap-addtabs.css" rel="stylesheet" />

    <script src="../../Scripts/jQuery/jquery.1.10.2.min.js"></script>    
    <script src="../../Scripts/bootstrap/v3.3.5/js/bootstrap.min.js"></script>    
    <script src="../../Scripts/bootstrap/tabs/bootstrap-addtabs.js"></script>
</head>
<body>
    <div id="tabs">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">主页</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
                欢迎
            </div>
        </div>
    </div>
</body>
</html>
<script src="../../../Scripts/common/ydlsoft.js"></script>
<script src="../../Scripts/sticky/jquery.sticky.min.js"></script>
<script src="../../Scripts/layer/layer.js"></script>
<script type="text/javascript">
    var ydlSoft = new YDLSoft();
    var _dataId = ydlSoft.GetQueryString("TabFrmID");
    $(function () {

        $('#tabs').addtabs();
        loadIndex = layer.msg('正在加载，请稍后', { icon: 16, shade: [0.8, '#393D49'] });
        jQuery.ajax({
            url: "../WebPage/DataService/TabPageService.ashx?method=GetTabsAllList&TabFrmID=" + _dataId,
            type: "post",
            success: function (text) {
                var dataObj = jQuery.parseJSON(text);
                if (dataObj.result) {
                    for (var i = 0, len = dataObj.data.length; i < len; i++) {
                        console.log(dataObj.data);
                        Addtabs.add({ id: dataObj.data[i]["TabID"], title: dataObj.data[i]["TabTitle"], url: dataObj.data[i]["TabUrl"] });                        
                    }
                    Addtabs.add({ id: dataObj.data[0]["TabID"], title: dataObj.data[0]["TabTitle"], url: dataObj.data[0]["TabUrl"] });
                }
                else {
                    tips(dataObj.message);
                }
                layer.close(loadIndex);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                tips("Ajax错误，加载失败!");
                layer.close(loadIndex);
            }
        });        
    })

    function tips(msg) {
        $.sticky({
            body: msg,
            position: 'top-mid',
            //useAnimateCss: true,
            closeable: false
        });
    }

</script>