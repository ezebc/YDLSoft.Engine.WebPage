﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using YDLSoft.DataBase.DataAcess;

namespace YDLSoft.Engine.WebPage.PlatFolder.SysExec
{
    public partial class CustomPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string pageId = Request["PageID"];
            if(!string.IsNullOrEmpty(pageId))
            {
                object o = SystemDB.ExecuteScalar("select PageHtml from plat_engine_webpage_custom_page where PageID='"+ pageId + "'");
                if(o != null)
                {
                    Response.Write(o.ToString());
                }else
                {
                    Server.Transfer("404.htm");
                }
            }
            else
            {
                Response.Write("缺少参数:PageID");
            }

        }
    }
}