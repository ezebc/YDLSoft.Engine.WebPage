﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="YDLSoft.Cloud.Service.Client.Admin.login" %>

<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <title>BCE-1.0 业务定制引擎</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">     
    <link rel="stylesheet" type="text/css" href="css/login.css">    
    <script src="../Scripts/jQuery/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/sticky/jquery.sticky.min.css">
    <script src="../Scripts/sticky/jquery.sticky.min.js"></script>
    <script src="js/login.min.js"></script>
</head>
<body onload="javascript:document.form1.UNAME.focus();" scroll="auto">
    <form name="form1">
        <div id="center">
            <div id="form">
                <div class="inputer">
                    <div class="user">
                        <input type="text" class="text" id="txtUserName" name="UNAME" maxlength="20" onmouseover="this.focus()" onfocus="this.select()" value="">
                    </div>
                    <div class="pwd">
                        <input type="password" class="text" id="txtPassword" name="PASSWORD" onmouseover="this.focus()" onfocus="this.select()" value="">
                    </div>
                    <div class="btn">
                        <input type="hidden" name="encode_type" value="1">
                        <input id="btn_login" onmouseenter="addClass(this,'submit-hover');" onmouseout="removeClass(this,'submit-hover');" type="button" onclick="Login();" class="submit" title="登录" value="">
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="bg-item-1"></div>
            <div class="bg-item-2"></div>
        </div>
        <div class="msg">
            <div></div>
            <div></div>
            <div></div>
            <div>
                <script language="JavaScript">
                    var allEmements=document.getElementsByTagName("*");
                    for(var i=0;i<allEmements.length;i++)
                    {
                        if(allEmements[i].tagName && allEmements[i].tagName.toLowerCase()=="iframe")
                        {
                            document.write("<div align='center' style='color:red;'><br><br><h2>系统提示：</h2><br><br>您的电脑可能感染了病毒或木马程序，请联系软件开发商寻求解决办法或下载360安全卫士查杀。<br>病毒网址（请勿访问）：<b><u>"+allEmements[i].src+"</u></b></div>");
                            allEmements[i].src="";
                        }
                    }
                </script>
            </div>
        </div>
    </form>
</body>
</html>