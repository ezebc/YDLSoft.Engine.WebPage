﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NavigateMain.aspx.cs" Inherits="YDLSoft.Cloud.Service.Client.Admin.NavigateMain" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="renderer" content="webkit" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>智能开发平台-业务定制</title>
    
    <link type="text/css" href="css/system.css" rel="stylesheet" id="skin" />
    <link href="../Scripts/jQuery/tabpanel/CSS/jquery.tabpanel.css" rel="stylesheet" />
    <style type="text/css">
        .target
        {
            position:absolute;
            left:200px;
            top:400px;
            border:solid 1px #ffccee;   
            padding:5px;
            background-color:Blue;
            color:#fff;
         }
         .icon_home
        {
            /*background-image:url("images/tabpanel/home.png");*/
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
     <div id="head">
	<div class="head">
		<a href="javascript:void(0)" class="info"><%=Session["personname"] %></a>
		<div class="usinfo">
			<ul class="l">
				<li><strong>个人信息</strong></li>
				<li>姓名：周超亿</li>
				<li>公司：<span>北京世纪建云科技有限公司</span>  </li>
				<li>部门：<span>研发中心</span></li>
				<li>角色：<span></span>  </li>
				<li>岗位：<span>平台组</span> </li>
			</ul>
			<ul class="c">
				<li><strong>我的业务</strong></li>
				<li><a href="javascript:void('0')" onclick="openPersonInfo2()">个人信息 &gt;</a></li>
				<li><a href="javascript:void('0')" id="changePassBtn">修改密码 &gt;</a></li>
				<li><a href="javascript:void('0')" onclick="openDayPlan()">日程安排 &gt;</a></li>
				<li><a href="javascript:void('0')" onclick="openPersonNote()">个人便签 &gt;</a></li>
				<li><a href="javascript:void('0')" onclick="openEndWorkflow()">处理完成 &gt;</a></li>
			</ul>
			<ul class="r">
				<li><strong>换肤</strong></li>
				<li class="k k1"></li>
				<li class="k k2"></li>
				<li class="k k3"></li>
				<li class="k k4"></li>
			</ul>
		</div>
		<a href="javascript:void(0)" class="action">快捷操作</a>
		<div class="actinfo">
			<ul>
				<a href="#" class="t">系统设置</a>
				<a href="http://www.yuntech.com.cn/yuntech/RichEditor/ueditor/Editor.aspx?type=add&userid=A5673E8D-F5FD-46A4-BCC9-B93E1EBBACC3&projectcode=yuntech_oa" target="_blank" >问题反馈单</a>
				<a href="javascript:void('0')" onclick='AddTab("在线人员", "onlinePersonList", "onlinePersonList", true, "scsexec/UncryptQueryConditionListRecord.aspx?sqlid=19300a15-d633-4289-9f65-b7efda7218b0", true)'>在线人员</a>
				<a href="javascript:void('0')" onclick='openCompNote()'>平台公告</a>
				<a href="javascript:void('0')" onclick='AddTab("通讯录", "addressBook", "addressBook", true, "SCSEXEC/UncryptQueryConditionListRecord2.aspx?SqlID=da711a1c-c806-4665-86c3-2afe923d100e", true)'>通讯录</a>
				<a href="http://www.yuntech.com.cn/demo/index.html" target="_blank">帮助手册</a>
				<a href="WebDownCenter/WebDownCenter.html" target="_blank">下载中心</a>
			</ul>
			<ul>
				<a href="#" class="t">常用工具</a>
				<a href="javascript:void('0')" onclick='AddTab("万年历", "wannianli", "wannianli", true, "SCSEXEC/calendar.htm", true)'>万年历</a>		
				<a href="javascript:void('0')" onclick='AddTab("计算器", "jisuanqi", "jisuanqi", true, "SCSEXEC/calculator.htm", true)'>计算器</a>
				<a href="javascript:void('0')" onclick='AddTab("度量衡换算", "dulianghuansuan", "dulianghuansuan", true, "workaspOA/tools/duliang2.htm", true)'>度量衡换算</a>
				<a href="javascript:void('0')" onclick='AddTab("常用邮编", "changyongyoubian", "changyongyoubian", true, "WorkAspOA/Postcode/Postcode_Areacode.aspx", true)'>常用邮编</a>
			</ul>
		</div>
		<a href="product.aspx?webid=000" target="_blank" class="return">前台</a>
        <a href="javascript:void(0)" onclick="refreshPage()" class="return" style="display:none">刷新</a>
		<a href="javascript:void(0)" onclick="ReLogin()" class="repeat">注销</a>
		<a href="product.aspx?webid=000" target="_blank" class="change" style="display:none" >进入前台</a>
		<div class="csystem" >
			
		</div>
	</div>
</div>
        <div id="temp" style="display:block;">
            <img src="images/admin_logo_s.png" />
        </div>

<div id="logo" style="display:none;">
	<div class="logo"></div>
	<div class="search">
		<div class="t">
			<a href="javascript:void(0)" class="a">找定义</a>
			<a href="javascript:void(0)">找帮助</a>
			<a href="javascript:void(0)">找日志</a>
		</div>
		<div class="m">
			<input type="text" />
			<a href="javascript:void(0)">搜索</a>
		</div>
	</div>
</div>

<div id="wrap">
	<a href="javascript:void(0)" id="hideLeftScrollBtn">开发导航</a>
	<div id="menu">
		<div class="menu">
			<a href="javascript:void(0)" class="t" id="hideLeftBtn">开发导航</a>
			<a href="javascript:void()">业务引擎</a>
			<a href="javascript:void()">数据引擎</a>
			<a href="javascript:void()">流程引擎</a>
            <a href="javascript:void()">权限引擎</a>
			<a href="javascript:void()">配套工具</a>
            <a href="javascript:void()">统计分析</a>
		</div>
	</div>
    
<div class="menulist">
<ul class="left">
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('表单模型', '001001', '52FA9F51-C558-462B-8DEB-F86BD00B850D', '1', '../PlatFolder/SysDefine/TableEngineList.html')">表单模型</a>
<a href="javascript:void()" onclick="AddTabs('表单模型', '001001', '52FA9F51-C558-462B-8DEB-F86BD00B850D', '1', '../PlatFolder/SysDefine/TableEngineList.aspx')">表单模型</a>
    <a href="javascript:void()" onclick="AddTabs('视图信息', '001021', '53FA9F51-C558-462B-8DEB-F86BD00B850D', '1', '../PlatFolder/SysDefine/ViewList.htm')">视图信息</a>
<a href="javascript:void()" onclick="AddTabs('反向导入','0267014','e5844123-c6d8-473f-be3b-b20354b86a8e','1','Develop/AppModelNewReverse.aspx')">反向导入</a>
    <a href="javascript:void()" onclick="AddTabs('字典设置', '0267014', 'e5844123-c6d8-473f-be3b-b20354b86a8e', '1', 'Develop/AppModelNewReverse.aspx')">字典设置</a>
</li>
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('导航模型','0266005','52387095-1d9d-48a0-ae4c-e0146a2b02f1','0','')">导航模型</a>
<a href="javascript:void()" onclick="AddTabs('系统页面定义','0266005002','2f1622ca-4ee2-4e07-8232-22a597daf494','1','http://www.yuntech.com.cn/develop/scsdef/Main.aspx')">系统页面定义</a>
<a href="javascript:void()" onclick="AddTabs('树形页面定义','0266005001','515d9fd5-9ec3-4860-930c-408162815b36','1','Develop/RelationDefineTreeMain.aspx')">树形页面定义</a>
<a href="javascript:void()" onclick="AddTabs('关联业务定义','0266005003','8926947e-5eca-493d-8ad3-72b24159d095','1','Develop/RelationDefineMain.aspx')">关联业务定义</a>
<a href="javascript:void()" onclick="AddTabs('框架页面定义','0266005004','957e3266-ec12-4582-9d6c-44d39179181d','1','Develop/AppModelContentMenuTreeClick.aspx?funid=9E288EBB-7F36-4497-AB3A-D5E45A09E993')">框架页面定义</a>
<a href="javascript:void()" onclick="AddTabs('导航图定义','0266005004','957e3266-ec12-4582-9d6c-44d39179181d','1','Develop/AppModelContentMenuTreeClick.aspx?funid=9E288EBB-7F36-4497-AB3A-D5E45A09E993')">导航图定义</a>
<a href="javascript:void()" onclick="AddTabs('导出Word定义','0266005004','957e3266-ec12-4582-9d6c-44d39179181d','1','Develop/WordDefineList.aspx')">导出Word定义</a>
</li>
</ul>
</div>
<div class="menulist">
<ul class="left">
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('自定义查询','0022016','3dd42385-9e6e-44b2-8c8a-7e893c3063a8','1','../PlatFolder/SysDefine/QuerySqlDefineListMiniUI.aspx')">自定义查询</a>
</li>
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('图表定义','0022017','7399752a-af94-4810-b690-94956f47c799','1','ChartDefineMain.aspx')">图表定义</a>
<a href="javascript:void()" onclick="AddTabs('FusionChar','0022018','7399752a-af94-4810-b690-94956f47c790','1','http://www.yuntech.com.cn/develop/scsdef/ChartView/ChartDataTableInfo.aspx')">FusionChar</a>

</li>
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('数据分析定义','0022012','df80321a-4e2e-493a-8361-4f5374bf6c8b','0','')">数据分析定义</a>
<a href="javascript:void()" onclick="AddTabs('Ebiao定义','0022012001','be502fcc-cc28-4399-aa0e-b226a12e4120','1','ebdefault.aspx')">Ebiao定义</a>
<a href="javascript:void()" onclick="AddTabs('多维分析定义','0022012002','d90a541d-1e1a-41d0-a06e-341b96b9ef0c','1','Develop/WebGridDefineMain.aspx')">多维分析定义</a>
</li>
</ul>
</div>
<div class="menulist">
<ul class="left">
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('流程设计','0206001','9a3384ac-f035-4abc-8915-34edc8949571','1','DefWorkFlow/DrawWorkFlow/Drawflowchart.application?url=http://www.yuntech.com.cn/oa_dev/DefWorkFlow/WorkFlowWebService.asmx?WSDL')">流程设计</a>
</li>
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('流程参数配置','0206002','88bc78d7-8ad6-4f50-94c7-c6ad2697524b','1','Develop/AppTableWorkFlowConfigure.aspx')">流程参数配置</a>
</li>
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('流程维护','0206003','04b8a873-b7f3-4118-a317-10b7a4e3ffd3','0','')">流程维护</a>
</li>
</ul>
</div>
    <div class="menulist">
        <ul class="left">
            <li>
                <a href="javascript:void()" class="t" onclick="AddTabs('流程设计','0206001','9a3384ac-f035-4abc-8915-34edc8949571','1','')">功能权限</a>
                <a href="javascript:void()" onclick="AddTabs('流程设计','0206001','9a3384ac-f035-4abc-8915-34edc8949571','1','')">部门权限</a>
                <a href="javascript:void()" onclick="AddTabs('流程设计','0206001','9a3384ac-f035-4abc-8915-34edc8949571','1','')">职务权限</a>
                <a href="javascript:void()" onclick="AddTabs('流程设计','0206001','9a3384ac-f035-4abc-8915-34edc8949571','1','')">角色权限</a>
                <a href="javascript:void()" onclick="AddTabs('流程设计','0206001','9a3384ac-f035-4abc-8915-34edc8949571','1','')">人员权限</a>
            </li>
            <li>
                <a href="javascript:void()" class="t" onclick="AddTabs('流程参数配置','0206002','88bc78d7-8ad6-4f50-94c7-c6ad2697524b','1','')">节点权限</a>
            </li>
            <li>
                <a href="javascript:void()" class="t" onclick="AddTabs('流程维护','0206003','04b8a873-b7f3-4118-a317-10b7a4e3ffd3','0','')">流程权限</a>
                <a href="javascript:void()" onclick="AddTabs('流程维护','0206003','04b8a873-b7f3-4118-a317-10b7a4e3ffd3','0','')">协同办公</a>
            </li>
        </ul>
    </div>
<div class="menulist">
<ul class="left">
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('日志记录','0022016','3dd42385-9e6e-44b2-8c8a-7e893c3063a8','1','2')">日志记录</a>
<a href="javascript:void()" onclick="AddTabs('用户操作日志','0267002','e29df190-a249-43f8-ba97-70f09a8376fe','1','Develop/loglist.html')">用户操作日志</a>
<a href="javascript:void()"  onclick="AddTabs('开发日志','0267006','698c3208-8a6f-4759-9b94-61451ee73b1b','1','2')">开发操作日志</a>
<a href="javascript:void()"  onclick="AddTabs('数据备份日志','0267010','8d623d81-87f1-4cc1-8510-82be5dc368d9','1','Develop/AppDataBackLogList.aspx')">数据备份日志</a>
<a href="javascript:void()"  onclick="AddTabs('数据删除日志','0267010','8d623d81-87f1-4cc1-8510-82be5dc368d9','1','Develop/AppSysDataDelLog.aspx')">数据删除日志</a>
</li>

<li>
<a href="javascript:void()" class="t" onclick="AddTabs('数据同步','0267004','11027801-9697-452c-a3df-fb8647aebd1b','1','Develop/SynDataList.aspx')">数据同步</a>
</li>
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('字段差异分析','0267005','c1364b24-b520-406c-a040-4d38a72aaf87','1','Develop/AppDifferAnalyseList.aspx')">字段差异分析</a>
</li>
<li>
<a href="javascript:void()" class="t" onclick="AddTabs('应用插件','0022016','3dd42385-9e6e-44b2-8c8a-7e893c3063a8','0','')">应用插件</a>
<a href="javascript:void()"  onclick="AddTabs('短信发送','0267003','949a47e0-880c-41e9-999c-68efd01ce949','1','2')">短信发送</a>
<a href="javascript:void()"  onclick="AddTabs('微信提醒','0267008','37ff7f30-c605-4c03-af58-6ac845894fd1','1','2')">微信提醒</a>
<a href="javascript:void()"  onclick="AddTabs('Excel导入','0267008','37ff7f30-c605-4c03-af58-6ac845894fd1','1','2')">Excel导入</a>
</li>
    </ul>
</div>

    <div class="menulist">
        <ul class="left">
            <li>
                <a href="javascript:void()" class="t" onclick="AddTabs('日志记录','0022016','3dd42385-9e6e-44b2-8c8a-7e893c3063a8','1','2')">日志统计</a>
                <a href="javascript:void()" onclick="AddTabs('登录日志','0267002','e29df190-a249-43f8-ba97-70f09a8376fe','1','Develop/loglist.html')">登录日志</a>
                <a href="javascript:void()" onclick="AddTabs('数据日志','0267002','e29df190-a249-43f8-ba97-70f09a8376fe','1','Develop/loglist.html')">数据日志</a>
                <a href="javascript:void()" onclick="AddTabs('开发日志','0267006','698c3208-8a6f-4759-9b94-61451ee73b1b','1','2')">开发操作日志</a>
                <a href="javascript:void()" onclick="AddTabs('数据备份日志','0267010','8d623d81-87f1-4cc1-8510-82be5dc368d9','1','Develop/AppDataBackLogList.aspx')">数据备份日志</a>
                <a href="javascript:void()" onclick="AddTabs('数据删除日志','0267010','8d623d81-87f1-4cc1-8510-82be5dc368d9','1','Develop/AppSysDataDelLog.aspx')">数据删除日志</a>
            </li>
            <li>
                <a href="javascript:void()" class="t" onclick="AddTabs('应用插件','0022016','3dd42385-9e6e-44b2-8c8a-7e893c3063a8','0','')">图表统计</a>
                <a href="javascript:void()" onclick="AddTabs('浏览器使用情况', '0267003', '949a47e0-880c-41e9-999c-68efd01ce949', '1', '2')">浏览器使用情况</a>
                <a href="javascript:void()" onclick="AddTabs('操作系统使用情况', '0267008', '37ff7f30-c605-4c03-af58-6ac845894fd1', '1', '2')">操作系统使用情况</a>
                <a href="javascript:void()" onclick="AddTabs('终端设备使用情况', '0267008', '37ff7f30-c605-4c03-af58-6ac845894fd1', '1', '2')">终端设备使用情况</a>
            </li>
        </ul>
    </div>

    
	<div id="main">
		<div id="tabs"></div>
	</div>
</div>

<script src="../Scripts/jQuery/jquery.min.js"></script>
<script src="../Scripts/jQuery/tabpanel/jquery.tabpanel.common.js"></script>
<script src="../Scripts/jQuery/tabpanel/jquery.tabpanel.js"></script>
<script type="text/javascript">   

    var tabWidth = document.documentElement.clientWidth;
    var tabHeight = document.documentElement.clientHeight;
    var IsFirstTab = true;

    jQuery(document).ready(function (e) {
        //参数说明见jQuery/jquery.tabpanel.js
        var items = [{ id: "999999", text: "导航页", isactive: true, url: "",classes: "icon_home"}];
        GetTabsSize();
        jQuery("#tabs").tabpanel({ items: items, width: tabWidth, height: tabHeight });
        AddTabFirstPage('导航页', '0', '0', false, "deskcenter.html?t="+Math.random(), false);
        var webid = "134";
        
        jQuery("#tabs").deltabitem("tab_li_999999");

        jQuery("#tabs").resizetabpanel(tabWidth, tabHeight);

    });
    function firstPage() {
        AddTabFirstPage('导航页', '1', '1', false, "deskcenter.html", false);
    }
    function TabsAutoFillSize() {
        GetTabsSize();
        jQuery("#tabs").resizetabpanel(tabWidth, tabHeight);
    }

    function GetTabsSize() {
        tabWidth = document.documentElement.clientWidth;
        tabHeight = document.documentElement.clientHeight;
        tabWidth = tabWidth - 200;
        tabHeight = tabHeight - 55 - 15;
        if (tabWidth == 0) {
            tabWidth = screen.availWidth - 5;
            tabHeight = screen.availHeight;
            if (window.top.frames["myFrames"].cols != "0,*") {
                var ColsStr = window.top.frames["myFrames"].cols;
                var arrCols = ColsStr.split(",");
                tabWidth = tabWidth - parseInt(arrCols[0]);
            }
        }
    }

    function AddTabs(funName, funwbs, funid, IsNavi, linkFile) {
        if (IsNavi == "1" && linkFile != "") {
            AddTab(funName, funwbs, funid, true, linkFile, true);
            _sys_fn.hideMenu(false);            
        }
    }

    //funName,funwbs,funid,
    function AddTab(funName, funwbs, funid, isClose, linkfile, isActive) {
        var url = "./DefaultMain_main.aspx?treeno=" + funwbs + "&funid=" + funid;
        if (isActive == undefined) isActive = true;
        if (linkfile != "") url = linkfile;
        if (funName == "导航页") {
            jQuery("#tabs").addtabitem({ id: 0, text: funName, classes: "icon_home", isactive: isActive, closeable: false, url: url });
            if (document.getElementById("tab_item_frame_0") != undefined)
                document.getElementById("tab_item_frame_0").src = url;
        }
        else if (funName == "个人桌面") {
            jQuery("#tabs").addtabitem({ id: 1, text: funName, classes: "icon_home", isactive: isActive, closeable: false, url: url });
            if (document.getElementById("tab_item_frame_1") != undefined)
                document.getElementById("tab_item_frame_1").src = url;
        }
        else {
            if (funid != "") {
                jQuery("#tabs").addtabitem({ id: funid, text: funName, isactive: true, closeable: true, url: url });
            }
            else {
                jQuery("#tabs").addtabitem({ id: funName, text: funName, isactive: true, closeable: true, url: url });
            }
        }
    }
    function addTabForDesk(funName, funid, url)
    {
        jQuery("#tabs").addtabitem({ id: funid, text: funName, isactive: true, closeable: true, url: url });
    }
    //首页
    function AddTabFirstPage(funName, funwbs, funid, isClose, fp_url, isActive) {
        AddTab(funName, funwbs, funid, false, fp_url, isActive);
    }

    jQuery(window).resize(function () {
        GetTabsSize();
        jQuery("#tabs").resizetabpanel(tabWidth, tabHeight);
    })

	</script> 

<script type="text/javascript" src="js/system.js"></script>
    </form>

    <div id="changeDutyBox" class="changebox">
		<div class="top">
			<div class="left">请选择列表中的岗位</div>
			<div class="right"><a href="javascript:void(0)"></a></div>
		</div>
		<div class="bot">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="8%" class="t">选择</td>
					<td width="18%" class="t">岗位名称</td>
					<td width="33%" class="t">部门</td>
					<td width="33%" class="t">公司</td>
					<td width="8%" class="t">默认</td>
				</tr>
				
			</table>
		</div>
	</div>
    <div id="changeCompanyBox" class="changebox">
		<div class="top">
			<div class="left">请选择列表公司</div>
			<div class="right"><a href="javascript:void(0)"></a></div>
		</div>
		<div class="bot">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="8%" class="t">选择</td>
					<td width="33%" class="t">公司名称</td>
				</tr>
				
			</table>
		</div>
	</div>
    <div id="changeRoleBox" class="changebox">
		<div class="top">
			<div class="left">请选择列表中的角色</div>
			<div class="right"><a href="javascript:void(0)"></a></div>
		</div>
		<div class="bot">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="8%" class="t">选择</td>
					<td width="33%" class="t">角色名称</td>
				</tr>
				
			</table>
		</div>
	</div>
    <div id="changeProjectBox" class="changebox">
		<div class="top">
			<div class="left">请选择列表中的项目</div>
			<div class="right"><a href="javascript:void(0)"></a></div>
		</div>
		<div class="bot">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="8%" class="t">选择</td>
					<td width="70%" class="t">项目名称</td>
					<td width="22%" class="t">项目编码</td>
				</tr>
				
			</table>
		</div>
	</div>
	<div class="littlebox" id="changePassBox">
		<div class="top">
			<div class="left">修改密码</div>
			<div class="right"><a href="javascript:void(0)"></a></div>
		</div>
		<div class="bot">
			<form name="passForm" action="" method="post">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="18%">旧密码</td>
					<td width="50%"><input id="txt_oldPass" type="password" class="ipt" /></td>
					<td width="32%">请输入旧密码</td>
				</tr>
				<tr>
					<td>新密码</td>
					<td><input id="txt_newPass" type="password" class="ipt" /></td>
					<td>请输入新密码</td>
				</tr>
				<tr>
					<td>重复密码</td>
					<td><input id="txt_newPassAgin" type="password" class="ipt" /></td>
					<td>请重新输入新密码</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><input type="button" value="修改密码" class="btn" onclick="openEditPass()" /></td>
					<td>&nbsp;</td>
				</tr>
			</table>
			</form>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
    function refreshPage(){

}
    //重新登录
    function ReLogin() {
        window.parent.location = "LoginOut.aspx";
    }
	//个人信息
    function openPersonInfo2() {
        AddTab("个人信息", "personInfo", "personInfo", true, "http://www.yuntech.com.cn/yuntech/Development_Interface/UpdateUserInfo.aspx?userid=97cb7974-2793-8d0f-d851-36d9cc091bac", true);
    }
	//修改密码
    function openEditPass() {
		var oldPass = document.getElementById("txt_oldPass").value;
		var newPass = document.getElementById("txt_newPass").value;
		var newPassAgin = document.getElementById("txt_newPassAgin").value;
		if(oldPass == "")
		{
			alert("请输入旧密码");
			return;
		}else if(newPass == "")
		{
			alert("请输入新密码");
			return;
		}else if(newPassAgin == "")
		{
			alert("请再次输入新密码");
			return;
		}
		
		if(newPass!=newPassAgin)
		{
			alert("两次输入的密码不一致");
			return;
		}		
		$.ajax({
			 type: 'GET',
			 cache:false,
			 url: 'data/PlatModelService.aspx?method=UpdatePass',
			 data: {'oldPass':oldPass, 'newPass': newPass},
			 success: function(data){
			    if(data != "")
				{
				var info = jQuery.evalJSON(data);
				alert(info.msg);
				}
			 },
			 error: function(request, textStatus, errorThrown){
				
			 }
		  });
	}
    //主风险管理消息
    function openZFxgl() {
        AddTab("主责风险管理", "zfxgl", "zfxgl", true, "SCSEXEC/UncryptQueryConditionListRecord.aspx?SqlID=544a0fc6-2fad-4a5a-9d22-2435c0ae0f3b&SqlQueryCondition=fxsjbh in (select fxsjbh from sg_fx_fxyjsz where charindex('',WarningEnduserID)>0)", true);
    }
	//与我相关风险管理消息
    function openFxgl() {
        AddTab("相关风险管理", "fxgl", "fxgl", true, "SCSEXEC/UncryptQueryConditionListRecord.aspx?SqlID=bce6b5bf-e3e1-4ede-9beb-33a544cad131&SqlQueryCondition=fxsjbh in (select fxsjbh from sg_fx_fxyjsz where charindex('',WarningAcceptID)>0 )", true);
    }
    //待办流程
    function openWaitWorkflow() {
        AddTab("待办流程", "waitWorkflow", "waitWorkflow", true, "WebWorkFlow/ListTask.aspx", true);
    }
	//已办流程
    function openEndWorkflow() {
        AddTab("已办流程", "endWorkflow", "endWorkflow", true, "SCSEXEC/UncryptQueryConditionListRecord.aspx?SqlID=40f97714-6815-4ace-9fc5-75b531dcc953", true);
    }
	//日程安排
    function openDayPlan() {
        AddTab("日程安排", "dayPlan", "dayPlan", true, "OA/workschedule/workScheduleZ.aspx", true);
    }
	//公司公告
    function openCompNote() {
        AddTab("公司公告", "compNote", "compNote", true, "SCSEXEC/UNCRYPTNewEditDelRecord.aspx?TblName=dt_Notes&NotAllowEdit=1", true);
    }
	//我的便签
    function openPersonNote() {
        AddTab("我的便签", "myNote", "myNote", true, "OA/WorkAspOA/OA_GRBQ/OA_grbq.aspx", true);
    }
    function chgcompany(compid) {
        //var gongsDS = Yuntech.UI.Product.Product.ChangeCompany(compid).value;
        //window.location.reload();
        //if (gongsDS == null)
        //    return;
    }
    function chgposition(gwid) {
        //var gongsDS = Yuntech.UI.Product.Product.ChangePosition(gwid).value;
        //window.location.reload();
        //if (gongsDS == null)
        //    return;
    }
    function SetDefaultPosition(positionid, deptid, compid) {
        //var ret_str = Yuntech.UI.Product.Product.SetDefaultPosition(positionid, deptid, compid).value;
    }
    function chgrole(RoleID) {
        var gongsDS = Yuntech.UI.Product.Product.ChangeRole(RoleID).value;
        window.location.reload();
        if (gongsDS == null)
            return;
    }
	function chgproject(ProjectID) {
        //var gongsDS = Yuntech.UI.Product.Product.ChangeProjectID(ProjectID).value;
        //window.location.reload();
        //if (gongsDS == null)
        //    return;
    }
    </script>
