﻿// JavaScript Document

$(function(){
	new System();
});
var isHidLeft = false;
/**
 * 系统
 */
var System = function(){
	this.dom = this.getDom();
	this.ie6 = $.browser.msie && $.browser.version == '6.0';
	this.logoFlag = false;
	this.wrapHeight = 0;
	this.wrapWidth = document.documentElement.clientWidth - 200;
	this.searchType = 0;
	this.init();
}
System.prototype = {
	
	init: function(){
		var _this = this;
		var timer = null;
		
		this.resize();
		
		window.onresize = function(){
			if(timer)
				clearTimeout(timer);
			timer = setTimeout(function(){_this.resize()}, 0);
		}
		
		if(this.ie6)
			this.dom.head.append(HTML.png);
		
		this.bind();
	},
	resize: function(){
		var h = this.logoFlag ? 130 : 60
			,ch = document.documentElement.clientHeight
			,oh = {height: (ch - h) + 'px'};
		
		this.dom.wrap.css(oh);
		
		this.wrapHeight = ch - h;
		
		if(this.ie6){
			this.dom.menu.css(oh); this.dom.main.css(oh);
		}
		
		this.dom.hideLeftScrollBtn[0].style.width = this.dom.hideLeftBtn[0].offsetWidth - 40 + 'px';
	},
	bind: function(){
		this.showUserInfo();
		this.skin();
		this.changeSkin();
		this.showActionInfo();
		this.showChangeSys();
		this.selSearchMod();
		//this.hideTop();
		this.hideLeft();
		this.showMenuList();
		
		var _this = this;
		this.dom.changeCompanyBtn.click(function(){
			_this.changeBox(_this.dom.changeCompanyBox);
		});
		this.dom.changeRoleBtn.click(function(){
			_this.changeBox(_this.dom.changeRoleBox);
		});
		this.dom.changeDutyBtn.click(function(){
			_this.changeBox(_this.dom.changeDutyBox);
		});
		this.dom.changePassBtn.click(function(){
			_this.dom.changePassBox.find(':password').val('');
			_this.changeBox(_this.dom.changePassBox);
		});
		this.dom.changeProjectBtn.click(function(){
			_this.changeBox(_this.dom.changeProjectBox);
		});
                this.dom.userInfoBox.find('.l span').each(function(){
			if($(this).text().length > 10){
				$(this).parent().attr('title', $(this).text());
				$(this).text($(this).text().substr(0, 10) + '..');
			}
		});
		if(this.dom.projectTextWrap.text().length > 30)
			this.dom.projectTextWrap.attr('title', this.dom.projectTextWrap.text()).text(this.dom.projectTextWrap.text().substr(0, 30) + '...');
	},
	showUserInfo: function(){
		var _this = this;
		var timer;
		var a = [this.dom.userInfo, this.dom.userInfoBox];
		
		for(var i = 0; i < a.length; i++){
			a[i].hover(
				function(){
					clearTimeout(timer);
					_this.dom.userInfo.addClass('infohover'); _this.dom.userInfoBox.slideDown('fast');
				},
				function(){
					timer = setTimeout(function(){
						_this.dom.userInfoBox.slideUp('fast', function(){
							_this.dom.userInfo.removeClass('infohover');
						});
					}, 100);
				}
			);
		}
	},
	skin: function(){
		var _this = this;
		
		this.dom.skins.hover(
			function(){
				$(this).animate({opacity: 1}, 'fast');
			},
			function(){
				$(this).animate({opacity: 0.7}, 'fast');
			}
		);
		
		this.dom.skins.each(function(i){
			$(this).click(function(){
				_this.dom.userInfoBox.slideUp('fast', function(){
					_this.dom.userInfo.removeClass('infohover');
					_this.dom.body.append(HTML.load); var load = $('#load'); load.css('top', '-25px').animate({top: '0px'}, 'fast');
					_this.addCookie('skin_scs', i);
					setTimeout(function(){
						load.animate({top: '-25px'}, 'fast', function(){
							$(this).remove();
							_this.changeSkin();
						});
					}, 1500);
				});
			});
		});
	},
	changeSkin: function(){
		var k = this.getCookie('skin_scs');
		if(k == null || k == 0){
			this.dom.skin.attr('href', 'css/system_1.css');
			//this.dom.logoImg[0].src = 'images/admin_logo.png';
		}else{
			this.dom.skin.attr('href', 'css/system_' + k + '.css');
			//this.dom.logoImg[0].src = 'images/admin_logo.png';
		}
	},
	addCookie: function(name, value){
		document.cookie = name + '=' + escape(value);
	},
	getCookie: function(name){
		var ret = '';
		var cookieArr = document.cookie.split(';');
		for(var i = 0; i < cookieArr.length; i++){
			var arr = cookieArr[i].split('=');
			// FF、Chrome的cookieName前有空格，IE没有
			if($.trim(arr[0]) == name){
				ret = unescape(arr[1]);
				break;
			}
		}
		return (ret == '' || ret == 'undefined') ? null : ret;
	},
	showActionInfo: function(){
		var _this = this;
		var timer;
		var a = [this.dom.action, this.dom.actionBox];
		
		for(var i = 0; i < a.length; i++){
			a[i].hover(
				function(){
					clearTimeout(timer);
					_this.dom.action.addClass('actionhover'); _this.dom.actionBox.slideDown('fast');
				},
				function(){
					timer = setTimeout(function(){
						_this.dom.actionBox.slideUp('fast', function(){
							_this.dom.action.removeClass('actionhover');
						});
					}, 100);
				}
			);
		}
	},
	showChangeSys: function(){
		var _this = this;
		var timer;
		var a = [this.dom.changeSys, this.dom.changeSysBox];
		
		for(var i = 0; i < a.length; i++){
			a[i].hover(
				function(){
					clearTimeout(timer);
					_this.dom.changeSys.addClass('changehover'); _this.dom.changeSysBox.slideDown('fast');
				},
				function(){
					timer = setTimeout(function(){
						_this.dom.changeSysBox.slideUp('fast', function(){
							_this.dom.changeSys.removeClass('changehover');
						});
					}, 100);
				}
			);
		}
	},
	selSearchMod: function(){
		var _this = this;
		this.dom.searchTabs.each(function(i){
			$(this).click(function(){
				_this.dom.searchTabs.eq(_this.searchType).removeClass('a');
				$(this).addClass('a');
				_this.searchType = i;
			});
		});
	},
	hideLeft: function(){
		var _this = this;
		
		this.dom.body.append(HTML.bar); var bar = $('#bar');
		
		if(this.ie6){
			bar[0].style.left = '0px'; bar[0].style.left = '-15px';
		}
		
		this.dom.hideLeftBtn.click(function(){
			_this.wrapWidth = document.documentElement.clientWidth;
			_this.dom.menu.css('z-index', 8);
			_this.dom.wrap.css('padding-left', '0px');
			_this.dom.menu.animate({left: '-200px'}, 'fast', function(){
				bar.animate({left: '0px'}, 'fast');
			});
			_this.dom.hideLeftScrollBtn.animate({left: '-200px'}, 'fast');
		});
		
		if(!this.ie6){
			bar.hover(
				function(){
					$(this).animate({opacity: 0.8}, 'fast');
				},
				function(){
					$(this).animate({opacity: 0.3}, 'fast');
				}
			);
		}
		
		bar.click(function(){
		    _this.wrapWidth = document.documentElement.clientWidth - 200;
			$(this).animate({left: '-15px'}, 'fast', function(){
				_this.dom.menu.animate({left: '0px'}, 'fast', function(){
					_this.dom.menu.css('z-index', 'auto');
					_this.dom.wrap.css('padding-left', '200px');
				});
				_this.dom.hideLeftScrollBtn.animate({left: '0px'}, 'fast');
			});
			jQuery("#tabs").resizetabpanel(_this.wrapWidth, _this.wrapHeight);
		});
		
		this.dom.hideLeftScrollBtn.click(function(){
			_this.dom.hideLeftBtn.trigger('click');
		});
	},
	showMenuList: function(){
		var _this = this;
		this.dom.menuTab.each(function(i){
			if(!i)
				return true;
			_this.menuBind($(this), _this.dom.menuList.eq(i - 1));
		});
	},
	menuBind: function(m, l){
		var _this = this;
		
		m.hover(
			function(){
				_this.resizeMenu(m, l);
			},
			function(){
				m[0].className = ''; l.hide();
			}
		);
		
		l.hover(
			function(){
				m[0].className = 'hover'; l.show();
			},
			function(){
				m[0].className = ''; l.hide();
			}
		);
	},
	resizeMenu: function(m, l){
		var menuTop = m[0].offsetTop;
		var scrollTop = this.dom.menu[0].scrollTop;
		var mh = 40;	// menuHeight
		var lh = 22;	// listPadding + listBorder
		
		m.addClass('hover'); l.show().css('height', 'auto');
		
		var listHeight = l[0].offsetHeight;
		var wrapHeight = this.wrapHeight;
		
		/**
		 *	1. 拉动了滚动条
		 *		1. 上部标记未完全隐藏(默认list位置对齐标记底部)
		 *			1. list高度大于menu的offsetTop高度
		 *				1. 计算list位置和高度
		 *			2. list高度小于menu的offsetTop高度
		 *				2. 计算list位置和高度(注意最后一个menu可能是半隐藏)
		 *		2. 上部标记完全隐藏(默认list位置上部对齐)
		 *			1. list高度大于menu的offsetTop高度
		 *				1. 计算list位置和高度
		 *			2. list高度小于menu的offsetTop高度
		 *				2. 计算list位置和高度(注意最后一个menu可能是半隐藏)
		 *	2. 未拉动滚动条(默认list位置对齐标记底部)
		 *		1. list高度大于menu的offsetTop高度
		 *			1. 计算list位置和高度
		 *		2. list高度小于menu的offsetTop高度
		 *			2. 计算list位置和高度(注意最后一个menu可能是半隐藏)
		 */
		
		var p = {
			top: '0px',
			height: 'auto'
		};
		
		if(scrollTop){
			if(scrollTop < mh){
				if(listHeight + (mh - scrollTop) >= menuTop - scrollTop + mh){
					p.top = mh - scrollTop + 'px';
					p.height = listHeight < wrapHeight - (mh - scrollTop) ? listHeight - lh + 'px' : wrapHeight - (mh - scrollTop) - lh + 'px';
				}else{
					if(menuTop - scrollTop + mh > wrapHeight)
						p.top = menuTop - scrollTop + mh - listHeight - (menuTop - scrollTop + mh - wrapHeight) + 'px';
					else
						p.top = menuTop - scrollTop + mh - listHeight + 'px';
				}
			}else{
				if(listHeight >= menuTop + mh - scrollTop)
					p.height = listHeight < wrapHeight ? listHeight - lh + 'px' : wrapHeight - lh + 'px';
				else{
					if(menuTop + mh - scrollTop > wrapHeight)
						p.top = menuTop + mh - scrollTop - listHeight - (menuTop + mh - scrollTop - wrapHeight) + 'px';
					else
						p.top = menuTop + mh - scrollTop - listHeight + 'px';
				}
			}
		}else{
			if(listHeight + mh >= menuTop + mh){
				p.top = mh + 'px';
				p.height = listHeight < wrapHeight - mh ? listHeight - lh + 'px' : wrapHeight - mh - lh + 'px';
			}else{
				if(menuTop + mh > wrapHeight)
					p.top = menuTop + mh - listHeight - (menuTop + mh - wrapHeight) + 'px';
				else
					p.top = menuTop + mh - listHeight + 'px';
			}
		}
		
		l.css(p);
	},
	changeBox: function(box){
		box.show();
		
		var clientWidth = document.documentElement.clientWidth;
		var clientHeight = document.documentElement.clientHeight;
		
		if(box[0].offsetHeight >= clientHeight)
			box.find('.bot').css({height: clientHeight - 100 + 'px', overflow: 'auto'});
		
		var boxTop = parseInt((clientHeight - box[0].offsetHeight) / 2);
		var boxLeft = parseInt((clientWidth - box[0].offsetWidth) / 2);
	
		box.css({top: boxTop + 'px', left: boxLeft + 'px'});
	
		this.dom.body.append(HTML.bg);
		var bg = $('#bg');
		bg.css({height: clientHeight + 'px', opacity: 0}).show().animate({opacity: 0.6}, 'slow');
		
		box.find('.top a').click(function(){
			box.hide();
			bg.animate({opacity: 0}, 'slow', function(){
				$(this).remove();
			});
		});
	},
	getDom: function(){
		return {
			head				: $('head'),
			userInfo			: $('#head .info'),
			userInfoBox			: $('#head .usinfo'),
			skins				: $('#head .k'),
			skin				: $('#skin'),
			logoImg				: $('#logo .logo img'),
			action				: $('#head .action'),
			actionBox			: $('#head .actinfo'),
			changeSys			: $('#head .change'),
			changeSysBox		: $('#head .csystem'),
			searchTabs			: $('#logo .search .t a'),
			hideTopBtn			: $('#hideTopBtn'),
			hideLeftBtn			: $('#hideLeftBtn'),
			hideLeftScrollBtn	: $('#hideLeftScrollBtn'),
			temp				: $('#temp'),
			logo				: $('#logo'),
			wrap				: $('#wrap'),
			menu				: $('#menu'),
			main				: $('#main'),
			menuTab				: $('#menu .menu a'),
			menuList			: $('#wrap .menulist'),
			changeCompanyBtn	: $('#changeCompanyBtn'),
			changeCompanyBox	: $('#changeCompanyBox'),
			changeRoleBtn		: $('#changeRoleBtn'),
			changeRoleBox		: $('#changeRoleBox'),
			changeDutyBtn		: $('#changeDutyBtn'),
			changeDutyBox		: $('#changeDutyBox'),
			changePassBtn		: $('#changePassBtn'),
			changePassBox		: $('#changePassBox'),
			changeProjectBtn	: $('#changeProjectBtn'),
			changeProjectBox	: $('#changeProjectBox'),
			projectTextWrap		: $('#projectTextWrap'),
			body				: $('body')
		}
	}
}

/**
 * 菜单
 */
var _sys_fn = {
	hideMenu: function(b){
		this.dom = this.getDom();
		var bar = $('#bar');
		
		this.dom.menuTab.removeClass('hover'); this.dom.menuList.hide();
		
		if(b){
			this.dom.menu.css('z-index', 8);
			this.dom.wrap.css('padding-left', '0px');
			this.dom.menu.animate({left: '-200px'}, 'fast', function(){
				bar.animate({left: '0px'}, 'fast');
			});
		}
	},
	getDom: function(){
		return {
			menuTab			: $('#menu .menu a'),
			menuList		: $('#wrap .menulist'),
			wrap			: $('#wrap'),
			menu			: $('#menu')
		}
	}
}

var HTML = {
	bg		: '<div id="bg"></div>',
	png		: '<script type="text/javascript" src="js/png.js"></script>\
				<script type="text/javascript">\
					DD_belatedPNG.fix("div, ul, img, li, input, a, dl, span, b, .png_bg");\
				</script>',
	bar		: '<a href="javascript:void(0)" id="bar"></a>',
	load	: '<div id="load">加载中...</div>'
}