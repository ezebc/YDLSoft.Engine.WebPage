﻿try { document.execCommand("BackgroundImageCache", false, true); } catch (e) { }
function getiev() {
    var userAgent = window.navigator.userAgent.toLowerCase();
    jQuery.browser.msie8 = jQuery.browser.msie && /msie 8\.0/i.test(userAgent);
    jQuery.browser.msie7 = jQuery.browser.msie && /msie 7\.0/i.test(userAgent);
    jQuery.browser.msie6 = !jQuery.browser.msie8 && !jQuery.browser.msie7 && jQuery.browser.msie && /msie 6\.0/i.test(userAgent);
    var v;
    if (jQuery.browser.msie8) {
        v = 8;
    }
    else if (jQuery.browser.msie7) {
        v = 7;
    }
    else if (jQuery.browser.msie6) {
        v = 6;
    }
    else { v = -1; }
    return v;
}
jQuery(document).ready(function () {
    var v = getiev()
    if (v > 0) {
        jQuery(document.body).addClass("ie ie" + v);
    }

});